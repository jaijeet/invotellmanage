package com.invotell.box.comn.exception;

public class InvoTellServiceException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvoTellServiceException(String message) {
		super(message);
	}

	public InvoTellServiceException(Throwable cause) {
		super(cause);
	}

	public InvoTellServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvoTellServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
