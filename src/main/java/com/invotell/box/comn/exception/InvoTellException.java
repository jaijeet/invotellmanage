package com.invotell.box.comn.exception;

public class InvoTellException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvoTellException(String message) {
		super(message);
	}

	public InvoTellException(Throwable cause) {
		super(cause);
	}

	public InvoTellException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvoTellException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
