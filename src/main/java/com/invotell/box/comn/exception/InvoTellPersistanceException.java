package com.invotell.box.comn.exception;

public class InvoTellPersistanceException extends InvoTellException{

	private static final long serialVersionUID = 1L;
	public InvoTellPersistanceException(String message) {
		super(message);
	}

	public InvoTellPersistanceException(Throwable cause) {
		super(cause);
	}

	public InvoTellPersistanceException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvoTellPersistanceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}