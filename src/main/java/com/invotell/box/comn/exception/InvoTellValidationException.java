package com.invotell.box.comn.exception;

import java.util.ArrayList;
import java.util.List;

import com.invotell.box.bs.model.InvoTellError;

public class InvoTellValidationException extends InvoTellException {

	private static final long serialVersionUID = 1L;

	private List<InvoTellError> invoTellErrors = new ArrayList<InvoTellError>(0);
	
	public InvoTellValidationException(String message) {
		super(message);
	}

	public InvoTellValidationException(Throwable cause) {
		super(cause);
	}

	public InvoTellValidationException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvoTellValidationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public List<InvoTellError> getInvoTellErrors() {
		return invoTellErrors;
	}

	public void setInvoTellErrors(List<InvoTellError> invoTellErrors) {
		this.invoTellErrors = invoTellErrors;
	}
}
