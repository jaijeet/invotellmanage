package com.invotell.box.bs.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InvoTellError implements Serializable{

	private static final long serialVersionUID = 1L;

	public static final Integer ERROR_BAD_REQUEST= 400;
	
	@JsonProperty("Code")
	private Integer code;
	
	@JsonProperty("Message")
	private String message;
}
