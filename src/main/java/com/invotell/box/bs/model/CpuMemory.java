package com.invotell.box.bs.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CpuMemory {
	@JsonProperty("ram")
	private Memory ram;
	@JsonProperty("swap")
	private Memory swap;
}
