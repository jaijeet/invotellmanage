package com.invotell.box.bs.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PbxQueue {
	private String keyName;
	private String type;
	private int padding;
	private Long qid;
	private Byte keyPress;
	private Integer parentNode;
	private Integer nodeId;
}
