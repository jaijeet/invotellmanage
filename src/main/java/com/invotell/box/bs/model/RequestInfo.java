package com.invotell.box.bs.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RequestInfo<T> implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty("RequestInfo")
	private transient T requestInfo;

}