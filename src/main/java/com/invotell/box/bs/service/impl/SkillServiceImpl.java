package com.invotell.box.bs.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invotell.box.bs.model.InvoTellError;
import com.invotell.box.bs.service.SkillService;
import com.invotell.box.comn.exception.InvoTellPersistanceException;
import com.invotell.box.comn.exception.InvoTellServiceException;
import com.invotell.box.comn.exception.InvoTellValidationException;
import com.invotell.box.dao.entity.Skill;
import com.invotell.box.dao.entity.SkillCategory;
import com.invotell.box.dao.modal.SkillModel;
import com.invotell.box.dao.repository.SkillCategoryRepository;
import com.invotell.box.dao.repository.SkillRepository;

@Service
public class SkillServiceImpl implements SkillService {
	private static final Logger LOGGER = LoggerFactory.getLogger(SkillServiceImpl.class);

	@Autowired
	private SkillRepository skillRepository;

	@Autowired
	private SkillCategoryRepository skillCategoryRepository;

	@Override
	public Skill addNewSkill(Skill skillToSave, Long categoryId)
			throws InvoTellServiceException, InvoTellValidationException {
		Skill result = null;
		try {
			if (skillToSave == null) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST,
						"Skill category details are required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			SkillCategory existingCat = skillCategoryRepository.getSkillCategoryBySkillId(categoryId);
			if (existingCat == null) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST,
						"Skill Category with skill category id [" + categoryId + "] doesn't exists");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			skillToSave.setSkillcategoryid(categoryId);
			result = skillRepository.saveNewSkill(skillToSave);

		} catch (InvoTellValidationException itve) {
			throw itve;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}
		return result;
	}

	@Override
	public Skill getExistingSkillById(Long skillId) throws InvoTellServiceException, InvoTellValidationException {
		Skill result = null;
		try {
			if (skillId == 0L || skillId == null) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Enter valid Skill ID");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			result = skillRepository.getSkillBySkillId(skillId,true);
		} catch (InvoTellPersistanceException ive) {
			LOGGER.error(ive.getMessage(), ive);
			throw new InvoTellServiceException(ive.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}
		return result;
	}

	@Override
	public Skill updateExistingSkill(Skill skill, Long skillId, Long skillcategoryid)
			throws InvoTellServiceException, InvoTellValidationException {
		Skill response = null;
		try {
			if (skillId == 0L || skillId == null) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Enter valid Skill ID");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			if (skillcategoryid == 0L || skillcategoryid == null) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Enter valid Skill Category ID");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			SkillCategory cat = skillCategoryRepository.getSkillCategoryBySkillId(skillcategoryid);
			if(cat==null) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Skill Category with skill category id="+skillcategoryid+" does not exist");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			Skill result = skillRepository.getSkillBySkillId(skillId,false);
			if (result != null) {
				response = skillRepository.updateSkill(skill, skillId, skillcategoryid);
			}else {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Skill with skill id="+skillId+" does not exist");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

		} catch (InvoTellPersistanceException ive) {
			LOGGER.error(ive.getMessage(), ive);
			throw new InvoTellServiceException(ive.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}
		return response;
	}

	@Override
	public void deleteExistingSkill(Long skillId) throws InvoTellServiceException, InvoTellValidationException {
		try {
			if (skillId == 0L || skillId == null) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Enter valid Skill ID");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			Skill result = skillRepository.getSkillBySkillId(skillId,false);
			if (result != null) {
				skillRepository.deletedSkillById(skillId);
			}else {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Skill with skill id="+skillId+" does not exist");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

		} catch (InvoTellPersistanceException ive) {
			LOGGER.error(ive.getMessage(), ive);
			throw new InvoTellServiceException(ive.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}
	}
	
	@Override
	public List<Skill> getSkillByCategoryId(Long catId) throws InvoTellServiceException, InvoTellValidationException {
		List<Skill> result=null;
		try {
			if (catId == 0L || catId == null) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Enter valid Skill Category ID");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			
			SkillCategory category = skillCategoryRepository.getSkillCategoryBySkillId(catId);
			if(category==null) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Skill categiry with id="+catId+" does not exist");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			
			result= skillRepository.getSkillListByCategoryId(catId);
		} catch (InvoTellPersistanceException ive) {
			LOGGER.error(ive.getMessage(), ive);
			throw new InvoTellServiceException(ive.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}
		return result;
	}
	
	@Override
	public List<SkillModel> getSkillList() throws InvoTellServiceException, InvoTellValidationException {
		List<SkillModel> result=null;
		try {
			result= skillRepository.getSkillList();
		} catch (InvoTellPersistanceException ive) {
			LOGGER.error(ive.getMessage(), ive);
			throw new InvoTellServiceException(ive.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}
		return result;
	}
}
