package com.invotell.box.bs.service.asterisk;

import java.io.IOException;
import java.util.HashMap;

import org.asteriskjava.live.ManagerCommunicationException;
import org.asteriskjava.manager.ManagerConnection;
import org.asteriskjava.manager.ManagerConnectionFactory;
import org.asteriskjava.manager.ManagerConnectionState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.invotell.box.dao.constant.CommonConstant;
import com.invotell.box.dao.repository.IVRInfoRepository;

public class AmiConnectionFactory {
	private static final Logger LOGGER = LoggerFactory.getLogger(IVRInfoRepository.class);
	
	private static HashMap<String, ManagerConnection> userManagerConnectionMap;
	
	public static synchronized ManagerConnection getManagerConnection() throws ManagerCommunicationException
    {

		LOGGER.info("inside AMIConnectionUtil.class-->getManagerConnection():" + "asteriskUrl=" + CommonConstant.ASTERISK_HOST + "," + "asteriskUserName=" + CommonConstant.ASTERISK_USERNAME + "," + "asteriskPassword=HIDDEN");
        ManagerConnectionFactory factory = new ManagerConnectionFactory(CommonConstant.ASTERISK_HOST, CommonConstant.ASTERISK_USERNAME, CommonConstant.ASTERISK_PASSWORD);
        ManagerConnection managerConnection = factory.createManagerConnection();
        LOGGER.debug("inside AMIConnectionUtil--> getManagerConnection(): , managerConnection=" + managerConnection);
        LOGGER.debug("Connected to Port: " + managerConnection.getPort() + " ManagerConnection is: " + managerConnection.toString());
        //LocalPort not available for some reason log.debug("Connected to Port: " + managerConnection.getPort() + " from LocalPort: " + managerConnection.getLocalPort());
        return managerConnection;
    }
	
	public static synchronized ManagerConnection getManagerConnection(String user) throws ManagerCommunicationException, IOException, Exception
    {
        if (userManagerConnectionMap == null)
        {
            userManagerConnectionMap = new HashMap<String, ManagerConnection>();
        }

        ManagerConnection localMC = userManagerConnectionMap.get(user);
        if (localMC == null)
        {
            localMC = getManagerConnection();
            localMC.login();
            userManagerConnectionMap.put(user, localMC);
        }
        else if (localMC.getState() != ManagerConnectionState.CONNECTED)
        {
            localMC = getManagerConnection();
            localMC.login();
            userManagerConnectionMap.put(user, localMC);
        }
        return localMC;
    }	
	
}
