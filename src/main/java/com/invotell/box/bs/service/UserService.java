package com.invotell.box.bs.service;

import java.util.List;

import com.invotell.box.comn.exception.InvoTellServiceException;
import com.invotell.box.comn.exception.InvoTellValidationException;
import com.invotell.box.comn.exception.NotFoundException;
import com.invotell.box.dao.entity.User;

public interface UserService {
	User addNewUser(User user) throws InvoTellServiceException,InvoTellValidationException;
	User getUserById(Long id) throws InvoTellServiceException,InvoTellValidationException,NotFoundException;
	User getUserByEmail(String email) throws InvoTellServiceException,InvoTellValidationException,NotFoundException;
	List<User> getAllUsers() throws InvoTellServiceException,NotFoundException;
	User updateUser(String email, User user) throws InvoTellServiceException,InvoTellValidationException,NotFoundException;
	void deleteUser(String email) throws InvoTellServiceException,InvoTellValidationException,NotFoundException;
}
