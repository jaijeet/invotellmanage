package com.invotell.box.bs.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invotell.box.bs.model.InvoTellError;
import com.invotell.box.bs.service.QueueInfoService;
import com.invotell.box.comn.exception.InvoTellServiceException;
import com.invotell.box.comn.exception.InvoTellValidationException;
import com.invotell.box.comn.exception.NotFoundException;
import com.invotell.box.dao.entity.IVR;
import com.invotell.box.dao.entity.Queue;
import com.invotell.box.dao.entity.QueueInfo;
import com.invotell.box.dao.repository.IVRRepository;
import com.invotell.box.dao.repository.QueueInfoRepository;
import com.invotell.box.dao.repository.QueueRepository;

@Service
public class QueueInfoServiceImpl implements QueueInfoService {

	private static final Logger LOGGER = LoggerFactory.getLogger(QueueInfoServiceImpl.class);
	
	@Autowired
	private IVRRepository ivrRepository;
	
	@Autowired
	private QueueRepository queueRepository;
	
	@Autowired
	private QueueInfoRepository queueInfoRepository;
	
	@Override
	public QueueInfo addNewQueueInfo(Long ivrId, Long qid, QueueInfo queueInfo)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addNewQueueInfoList(Long ivrId, Long qid, List<QueueInfo> listToAdd)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException {
		
	}
	
	@Override
	public QueueInfo getQueueInfo(Long ivrId, Long qid, String keyword)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteQueueInfo(Long ivrId, Long qid, String keyword)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException {
		// TODO Auto-generated method stub

	}

	@Override
	public QueueInfo updateQueueInfo(Long ivrId, Long qid, String keyword, QueueInfo QueueInfo)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateQueueInfoList(Long ivrId, Long qid, List<QueueInfo> listToUpdate)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException {
		
	}
	
	@Override
	public void addQueueInfoList(Long ivrId, Long qid, List<QueueInfo> listToUpdate)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException {
		try {
			if (ivrId == null || ivrId == 0L) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "IVRID is required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			if (qid == null || qid == 0L) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "IVRID is required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			
			IVR existedIvr = ivrRepository.getIvrByIvrId(ivrId, false, false);
			if (existedIvr == null) {
				NotFoundException notFoundExcp = new NotFoundException("IVR with ivrID [" + ivrId + "] not found");
				throw notFoundExcp;
			}
			
			Queue queue = queueRepository.getQueue(ivrId, qid);
			if (queue == null) {
				NotFoundException notFoundExcp = new NotFoundException("Queue with qid [" + qid + "] not found");
				throw notFoundExcp;
			}
			
		} catch (InvoTellValidationException validationException) {
			throw validationException;
		} catch (NotFoundException notFoundExcp) {
			throw notFoundExcp;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e);
		}
		
	}

}
