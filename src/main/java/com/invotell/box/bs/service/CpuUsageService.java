package com.invotell.box.bs.service;

import java.util.List;

import com.invotell.box.bs.model.CpuMemory;
import com.invotell.box.comn.exception.InvoTellServiceException;
import com.invotell.box.dao.entity.CPU;

public interface CpuUsageService {
	List<CPU> getAllCpuUsage() throws InvoTellServiceException;
	CpuMemory getCpuMemoryUsage() throws InvoTellServiceException;
}
