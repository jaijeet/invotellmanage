package com.invotell.box.bs.service;

import java.util.List;

import com.invotell.box.comn.exception.InvoTellServiceException;
import com.invotell.box.comn.exception.InvoTellValidationException;
import com.invotell.box.dao.entity.Skill;
import com.invotell.box.dao.modal.SkillModel;

public interface SkillService {
	Skill addNewSkill(Skill skill,Long categoryId) throws InvoTellServiceException,InvoTellValidationException;
	Skill getExistingSkillById(Long skillId) throws InvoTellServiceException,InvoTellValidationException;
	Skill updateExistingSkill(Skill skill,Long skillId,Long skillcategoryid) throws InvoTellServiceException,InvoTellValidationException;
	void deleteExistingSkill(Long skillId) throws InvoTellServiceException,InvoTellValidationException;
	List<Skill> getSkillByCategoryId(Long catId) throws InvoTellServiceException,InvoTellValidationException;
	List<SkillModel> getSkillList() throws InvoTellServiceException,InvoTellValidationException;
}
