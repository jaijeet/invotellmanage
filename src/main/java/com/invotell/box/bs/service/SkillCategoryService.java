package com.invotell.box.bs.service;

import java.util.List;

import com.invotell.box.comn.exception.InvoTellServiceException;
import com.invotell.box.comn.exception.InvoTellValidationException;
import com.invotell.box.dao.entity.SkillCategory;

public interface SkillCategoryService {
	public SkillCategory addNewSkillCategory(SkillCategory category) throws InvoTellServiceException,InvoTellValidationException;;
	public void updateSkillCategory(Long skillCategoryId,SkillCategory category) throws InvoTellServiceException, InvoTellValidationException;
	public SkillCategory getskillCategoryById(Long skillCategoryId) throws InvoTellServiceException, InvoTellValidationException ;
	public SkillCategory getskillCategoryByName(String name) throws InvoTellServiceException, InvoTellValidationException ;
	public List<SkillCategory> getAllSkillCategory(boolean childLoad) throws InvoTellServiceException, InvoTellValidationException ;
	public void deleteSkillCategory(Long catId,String name) throws InvoTellServiceException, InvoTellValidationException ;
}
	