package com.invotell.box.bs.service;

import java.util.List;

import com.invotell.box.comn.exception.InvoTellServiceException;
import com.invotell.box.comn.exception.InvoTellValidationException;
import com.invotell.box.comn.exception.NotFoundException;
import com.invotell.box.dao.entity.IVRInfo;

public interface IVRInfoService {
	IVRInfo addNewIvrInfo (Long id, IVRInfo ivr) throws InvoTellServiceException,InvoTellValidationException,NotFoundException;
	IVRInfo getIvrInfo (Long id, String keyword) throws InvoTellServiceException,InvoTellValidationException,NotFoundException;
	void deleteIvrInfo (Long id, String keyword) throws InvoTellServiceException,InvoTellValidationException,NotFoundException;
	IVRInfo updateIvrInfo (Long id, String keyword, IVRInfo ivrInfo) throws InvoTellServiceException,InvoTellValidationException,NotFoundException;
	void updateIvrInfoList(Long ivrId,List<IVRInfo> listToUpdate) throws InvoTellServiceException, InvoTellValidationException, NotFoundException;
}
