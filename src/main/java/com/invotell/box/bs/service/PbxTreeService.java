package com.invotell.box.bs.service;

import java.util.List;

import com.invotell.box.bs.model.PbxQueue;
import com.invotell.box.comn.exception.InvoTellServiceException;
import com.invotell.box.dao.entity.PbxIvrTree;

public interface PbxTreeService {
	public PbxIvrTree addPbxIvrTree(PbxIvrTree pbxIvrTree) throws InvoTellServiceException;
	public List<PbxQueue> getPbxTreeByIvrId(Long ivrID) throws InvoTellServiceException;
}
