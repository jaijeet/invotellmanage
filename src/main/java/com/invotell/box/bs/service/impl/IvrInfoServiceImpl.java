package com.invotell.box.bs.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invotell.box.bs.model.InvoTellError;
import com.invotell.box.bs.service.IVRInfoService;
import com.invotell.box.comn.exception.InvoTellServiceException;
import com.invotell.box.comn.exception.InvoTellValidationException;
import com.invotell.box.comn.exception.NotFoundException;
import com.invotell.box.dao.entity.IVR;
import com.invotell.box.dao.entity.IVRInfo;
import com.invotell.box.dao.repository.IVRInfoRepository;
import com.invotell.box.dao.repository.IVRRepository;

@Service
public class IvrInfoServiceImpl implements IVRInfoService {

	private static final Logger LOGGER = LoggerFactory.getLogger(IvrInfoServiceImpl.class);

	@Autowired
	private IVRRepository ivrRepository;

	@Autowired
	private IVRInfoRepository ivrInfoRepository;

	@Override
	public IVRInfo addNewIvrInfo(Long ivrID, IVRInfo ivrInfoToSave)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException {
		IVRInfo info = null;
		try {
			if (ivrID == null || ivrID == 0L) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "IVRID is required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			IVR existedIvr = ivrRepository.getIvrByIvrId(ivrID, false, false);
			if (existedIvr == null) {
				NotFoundException notFoundExcp = new NotFoundException("IVR with ivrID [" + ivrID + "] not found");
				throw notFoundExcp;
			}

			ivrInfoToSave.setIvrID(existedIvr.getId());
			info = ivrInfoRepository.saveIvrInfo(ivrInfoToSave, true);

		} catch (InvoTellValidationException validationException) {
			throw validationException;
		} catch (NotFoundException notFoundExcp) {
			throw notFoundExcp;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e);
		}
		return info;
	}

	@Override
	public IVRInfo getIvrInfo(Long ivrID, String keyword)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException {
		IVRInfo info = null;
		try {

			if (ivrID == null || ivrID == 0L) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "IVRID is required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			IVR existedIvr = ivrRepository.getIvrByIvrId(ivrID, false, false);
			if (existedIvr == null) {
				NotFoundException notFoundExcp = new NotFoundException("IVR with ivrID [" + ivrID + "] not found");
				throw notFoundExcp;
			}

			info = ivrInfoRepository.getIvrInfo(existedIvr.getIvrID(), keyword, true);

		} catch (InvoTellValidationException validationException) {
			throw validationException;
		} catch (NotFoundException notFoundExcp) {
			throw notFoundExcp;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e);
		}
		return info;
	}

	@Override
	public void deleteIvrInfo(Long ivrID, String keyword)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException {
		try {
			if (ivrID == null || ivrID == 0L) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "IVRID is required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			IVR existedIvr = ivrRepository.getIvrByIvrId(ivrID, false, false);
			if (existedIvr == null) {
				NotFoundException notFoundExcp = new NotFoundException("IVR with ivrID [" + ivrID + "] not found");
				throw notFoundExcp;
			}

			IVRInfo ivrInfo = ivrInfoRepository.getIvrInfo(existedIvr.getId(), keyword, false);
			if (ivrInfo == null) {
				NotFoundException notFoundExcp = new NotFoundException(
						"IVRinfo with ivrID [" + ivrID + "] and keyword [" + keyword + "] not found");
				throw notFoundExcp;
			}

			ivrInfoRepository.deleteIvrInfo(ivrInfo, true);

		} catch (InvoTellValidationException validationException) {
			throw validationException;
		} catch (NotFoundException notFoundExcp) {
			throw notFoundExcp;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e);
		}
	}

	@Override
	public IVRInfo updateIvrInfo(Long ivrID, String keyword, IVRInfo ivrInfo)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException {
		IVRInfo info = null;
		try {

			if (ivrID == null || ivrID == 0L) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "IVRID is required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			IVR existedIvr = ivrRepository.getIvrByIvrId(ivrID, false, false);
			if (existedIvr == null) {
				NotFoundException notFoundExcp = new NotFoundException("IVR with ivrID [" + ivrID + "] not found");
				throw notFoundExcp;
			}

			info = ivrInfoRepository.getIvrInfo(ivrID, keyword, false);
			if (info == null) {
				NotFoundException notFoundExcp = new NotFoundException(
						"IVRinfo with ivrID [" + ivrID + "] and keyword [" + keyword + "] not found");
				throw notFoundExcp;
			}
			ivrInfoRepository.updateIvrInfo(info, ivrInfo, true);

		} catch (InvoTellValidationException validationException) {
			throw validationException;
		} catch (NotFoundException notFoundExcp) {
			throw notFoundExcp;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e);
		}
		return info;
	}

	@Override
	public void updateIvrInfoList(Long ivrID, List<IVRInfo> listToUpdate)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException {
		try {
			if (ivrID == null || ivrID == 0L) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "IVRID is required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			IVR existedIvr = ivrRepository.getIvrByIvrId(ivrID, false, false);
			if (existedIvr == null) {
				NotFoundException notFoundExcp = new NotFoundException("IVR with ivrID [" + ivrID + "] not found");
				throw notFoundExcp;
			}
			this.checkForHolidays(listToUpdate, ivrID);
			ivrInfoRepository.updateIvrInfoList(ivrID, listToUpdate, true);

		} catch (InvoTellValidationException validationException) {
			throw validationException;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e);
		}
	}

	private void checkForHolidays(List<IVRInfo> listToUpdate, Long ivrID) {
		try {
			ivrInfoRepository.deleteHolidayList(ivrID, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void copyIvrEntity(IVRInfo existingIvrInfo, IVRInfo toUpdate) {
		existingIvrInfo.setKeyword(toUpdate.getKeyword());
		existingIvrInfo.setValue(toUpdate.getValue());
	}
}
