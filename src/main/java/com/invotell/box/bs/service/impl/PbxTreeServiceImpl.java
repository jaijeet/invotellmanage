package com.invotell.box.bs.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invotell.box.bs.model.InvoTellError;
import com.invotell.box.bs.model.PbxQueue;
import com.invotell.box.bs.service.PbxTreeService;
import com.invotell.box.comn.exception.InvoTellPersistanceException;
import com.invotell.box.comn.exception.InvoTellServiceException;
import com.invotell.box.comn.exception.InvoTellValidationException;
import com.invotell.box.dao.entity.IVR;
import com.invotell.box.dao.entity.PbxIvrTree;
import com.invotell.box.dao.repository.IVRRepository;
import com.invotell.box.dao.repository.PbxIvrTreeRepository;

@Service
public class PbxTreeServiceImpl implements PbxTreeService {
	
	private List<PbxQueue> pbxList;

	@Autowired
	private PbxIvrTreeRepository pbxTreeRepository;
	
	@Autowired
	private IVRRepository ivrRepository;
	
	@Override
	public PbxIvrTree addPbxIvrTree(PbxIvrTree pbxIvrTree) throws InvoTellServiceException {
		PbxIvrTree result = null;
		try {
			Integer nodeId = pbxTreeRepository.getMaxNodeId(pbxIvrTree.getIvrid());
			if(nodeId==null) {
				nodeId=0;
			}else {
				nodeId++;
			}
			pbxIvrTree.setNodeid(nodeId);
			result = pbxTreeRepository.savePbxTree(pbxIvrTree);			
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new InvoTellServiceException(e);
		}
		return result;
	}
	
	@Override
	public List<PbxQueue> getPbxTreeByIvrId(Long ivrID) throws InvoTellServiceException{
		this.pbxList = new ArrayList<PbxQueue>();
		try {
			if (ivrID == null) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Ivr details are required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			
			IVR existingIvr = ivrRepository.getIvrByIvrId(ivrID,false,false);
			if(existingIvr==null)
			{
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Ivr with ivrID ["+ivrID+"] doesn't exists");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			List<PbxIvrTree> pbxIvrTrees = pbxTreeRepository.getPbxIvrTreeList(ivrID);
			displayIvrTree(null, 0, pbxIvrTrees, ivrID);
		} catch (Exception e) {
			e.printStackTrace();
			throw new InvoTellServiceException(e);
		}
		return pbxList;
	}

	
	private void displayIvrTree(Integer parentNode,int padding, List<PbxIvrTree> pbxIvrTrees, Long ivrId){
		for(PbxIvrTree ivrTree: pbxIvrTrees) {
			if(parentNode == null && ivrTree.getParenetNode()==null) {
				displayIvrTree(ivrTree.getNodeid(), padding+1, pbxIvrTrees, ivrId);
			}
			if(parentNode!=null && ivrTree.getParenetNode()!=null && ivrTree.getParenetNode().equals(parentNode)) {
				PbxQueue queue = new PbxQueue();
				queue.setKeyName(ivrTree.getKeyPress()+" - "+ivrTree.getName());
				queue.setPadding(padding);
				queue.setQid(ivrTree.getQid());
				queue.setType(ivrTree.getNodeType());
				queue.setKeyPress(ivrTree.getKeyPress());
				queue.setParentNode(ivrTree.getParenetNode());
				queue.setNodeId(ivrTree.getNodeid());
				this.pbxList.add(queue);
				displayIvrTree(ivrTree.getNodeid(), padding+1, pbxIvrTrees, ivrId);
			}
		}
	}
	
}
