package com.invotell.box.bs.service.helper;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import org.springframework.stereotype.Component;

import com.invotell.box.dao.constant.IvrConstantEnum;
import com.invotell.box.dao.entity.IVRInfo;
import com.invotell.box.dao.entity.PbxIvrTree;
import com.invotell.box.dao.entity.Queue;

@Component
public class IvrServiceHelper {
	public List<IVRInfo> craeteIvrInfo(Long ivrID) {
		List<IVRInfo> infos = new ArrayList<IVRInfo>();
		EnumSet.allOf(IvrConstantEnum.class)
        .forEach(ivrInfoKeyword -> {
        	if(!ivrInfoKeyword.getValue().equals("holiday")) {
        		IVRInfo info = new IVRInfo();
            	info.setIvrID(ivrID);
            	info.setKeyword(ivrInfoKeyword.getValue());
            	info.setValue(ivrInfoKeyword.getDefaultVal());
            	infos.add(info);
        	}
        });
		return infos;
	}
	
	public Queue createDefaultQueue(long ivrId) {
		Queue queue = new Queue();
		queue.setQueueName("Default");
		queue.setOnKey((byte)11);
		queue.setQueueID(ivrId);
		return queue;
	}
	
	
	public PbxIvrTree createDefaultPbxIvrTree(Queue queue) {
		PbxIvrTree pbxIvrTree = new PbxIvrTree();
		pbxIvrTree.setNodeType("BRANCH");
		pbxIvrTree.setName("ROOT");
		pbxIvrTree.setKeyPress((byte)11);
		pbxIvrTree.setNodeid(0);
		pbxIvrTree.setPromptStatus(false);
		pbxIvrTree.setQid(queue.getQueueID());
		return pbxIvrTree;
	}
	
	public PbxIvrTree createPbxIvrTreeByQueue(Queue queue,Integer parentNode,boolean isVoicePrompt,boolean promptStatus) {
		String nodeType= (isVoicePrompt)?"Voice Prompt":"Queue";
		PbxIvrTree pbxIvrTree = new PbxIvrTree();
		pbxIvrTree.setNodeType(nodeType);
		pbxIvrTree.setName(queue.getQueueName());
		pbxIvrTree.setKeyPress(queue.getOnKey());
		pbxIvrTree.setNodeid(0);
		pbxIvrTree.setParenetNode(parentNode);
		pbxIvrTree.setPromptStatus(promptStatus);
		pbxIvrTree.setQid(queue.getQueueID());
		return pbxIvrTree;
	}
}
