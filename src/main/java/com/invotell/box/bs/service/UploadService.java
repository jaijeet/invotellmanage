package com.invotell.box.bs.service;

import org.springframework.web.multipart.MultipartFile;

import com.invotell.box.comn.exception.InvoTellServiceException;
import com.invotell.box.comn.exception.InvoTellValidationException;
import com.invotell.box.comn.exception.NotFoundException;

public interface UploadService {
	public String uploadIvrMusicFiles(MultipartFile files,Long  ivrID, String keyword) throws InvoTellServiceException,InvoTellValidationException,NotFoundException;
	public String uploadQueuePromptFile(MultipartFile files,Long  ivrID, Long qid) throws InvoTellServiceException,InvoTellValidationException,NotFoundException;
}
