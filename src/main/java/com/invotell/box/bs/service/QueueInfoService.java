package com.invotell.box.bs.service;

import java.util.List;

import com.invotell.box.comn.exception.InvoTellServiceException;
import com.invotell.box.comn.exception.InvoTellValidationException;
import com.invotell.box.comn.exception.NotFoundException;
import com.invotell.box.dao.entity.QueueInfo;

public interface QueueInfoService {
	QueueInfo addNewQueueInfo (Long ivrId,Long qid, QueueInfo queueInfo) throws InvoTellServiceException,InvoTellValidationException,NotFoundException;
	void addNewQueueInfoList (Long ivrId,Long qid, List<QueueInfo> listToAdd) throws InvoTellServiceException,InvoTellValidationException,NotFoundException;
	QueueInfo getQueueInfo (Long ivrId,Long qid, String keyword) throws InvoTellServiceException,InvoTellValidationException,NotFoundException;
	void deleteQueueInfo (Long ivrId,Long qid, String keyword) throws InvoTellServiceException,InvoTellValidationException,NotFoundException;
	QueueInfo updateQueueInfo (Long ivrId,Long qid, String keyword, QueueInfo QueueInfo) throws InvoTellServiceException,InvoTellValidationException,NotFoundException;
	void addQueueInfoList(Long ivrId,Long qid,List<QueueInfo> listToUpdate) throws InvoTellServiceException, InvoTellValidationException, NotFoundException;
	void updateQueueInfoList(Long ivrId,Long qid,List<QueueInfo> listToUpdate) throws InvoTellServiceException, InvoTellValidationException, NotFoundException;
}
