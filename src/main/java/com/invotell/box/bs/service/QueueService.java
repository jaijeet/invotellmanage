package com.invotell.box.bs.service;

import com.invotell.box.comn.exception.InvoTellServiceException;
import com.invotell.box.comn.exception.InvoTellValidationException;
import com.invotell.box.comn.exception.NotFoundException;
import com.invotell.box.dao.entity.Queue;

public interface QueueService {
	Queue addNewQueue (Long ivrID, Queue ivr) throws InvoTellServiceException,InvoTellValidationException,NotFoundException;
	Queue getQueue (Long ivrID, Long queueId) throws InvoTellServiceException,InvoTellValidationException,NotFoundException;
	void deleteQueue (Long ivrID, Long queueId) throws InvoTellServiceException,InvoTellValidationException,NotFoundException;
}
