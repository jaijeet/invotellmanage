package com.invotell.box.bs.service.impl;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.invotell.box.bs.model.InvoTellError;
import com.invotell.box.bs.service.IVRInfoService;
import com.invotell.box.bs.service.IVRService;
import com.invotell.box.bs.service.UploadService;
import com.invotell.box.comn.exception.InvoTellServiceException;
import com.invotell.box.comn.exception.InvoTellValidationException;
import com.invotell.box.comn.exception.NotFoundException;
import com.invotell.box.dao.constant.UploadFileConstant;
import com.invotell.box.dao.entity.IVR;
import com.invotell.box.dao.entity.IVRInfo;

@Service
public class UploadServiceImpl implements UploadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UploadServiceImpl.class);

	@Autowired
	private IVRService ivrService;

	@Autowired
	private IVRInfoService ivrInfoService;

	@Override
	public String uploadIvrMusicFiles(MultipartFile files, Long ivrID, String keyword)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException {
		String originalFileName = null;
		boolean successUpload = false;
		String uploadFileName = null;
		boolean isNewIvrInfo = false;
		try {

			if (ivrID == null) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Enter valid IVR ID");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			if (files == null) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Uploaded file is missing");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			IVR existedIvr = ivrService.getIvrByIdWithoutIvrInfo(ivrID, false, false);
			if (existedIvr == null) {
				throw new NotFoundException("Enter valid IVR ID");
			}

			IVRInfo ivrInfo = ivrInfoService.getIvrInfo(ivrID, keyword);
			if (ivrInfo == null) {
				ivrInfo = new IVRInfo();
				ivrInfo.setKeyword(keyword);
				ivrInfo.setIvrID(ivrID);
				isNewIvrInfo = true;
			}

			if (keyword.contains("welcome")) {
				uploadFileName = UploadFileConstant.WELCOME_FILE_NAME + "."
						+ files.getOriginalFilename().split("[.]")[1];
				originalFileName = uploadDocument(files, UploadFileConstant.UPLOAD_IVR_MUSIC_DIR, uploadFileName);
				successUpload = true;
				ivrInfo.setValue(originalFileName);
			} else if (keyword.contains("Hold")) {
				uploadFileName = UploadFileConstant.HOLD_FILE_NAME + "." + files.getOriginalFilename().split("[.]")[1];
				originalFileName = uploadDocument(files, UploadFileConstant.UPLOAD_IVR_MUSIC_DIR, uploadFileName);
				ivrInfo.setValue(originalFileName);
				successUpload = true;
			} else if (keyword.contains("nightmode")) {
				uploadFileName = UploadFileConstant.HOLD_FILE_NAME + "." + files.getOriginalFilename().split("[.]")[1];
				originalFileName = uploadDocument(files, UploadFileConstant.UPLOAD_IVR_MUSIC_DIR, uploadFileName);
				ivrInfo.setValue(originalFileName);
				successUpload = true;
			} else {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Enter the valid keywrord");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			if (successUpload) {
				updateIvrInfoMusic(ivrInfo, existedIvr.getIvrID(), isNewIvrInfo);
			} else {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Not able to proceed upload");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			return "SuccessFully uploaded";
		} catch (InvoTellValidationException validationException) {
			throw validationException;
		} catch (NotFoundException notFoundExcp) {
			throw notFoundExcp;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e);
		}
	}

	@Override
	public String uploadQueuePromptFile(MultipartFile files, Long ivrID, Long qid)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException {
		String uploadFileName = null;
		try {
			if (ivrID == null) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Enter valid IVR ID");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			if (files == null) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Uploaded file is missing");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			IVR existedIvr = ivrService.getIvrByIdWithoutIvrInfo(ivrID, false, false);
			if (existedIvr == null) {
				throw new NotFoundException("Enter valid IVR ID");
			}

			uploadFileName = UploadFileConstant.QUEUE_PROMPT +"-"+ivrID+"-"+qid+ "."+ files.getOriginalFilename().split("[.]")[1];
			uploadDocument(files, UploadFileConstant.UPLOAD_QUEUE_PROMPT_DIR, uploadFileName);
					
		} catch (InvoTellValidationException validationException) {
			throw validationException;
		} catch (NotFoundException notFoundExcp) {
			throw notFoundExcp;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e);
		}
		return "SuccessFully uploaded";
	}

	private String uploadDocument(MultipartFile multipartFile, String path, String name)
			throws InvoTellServiceException {
		String originalfileName = null;
		try {
			Path fileNamePath = Paths.get(path, name);

			File f = new File(path + name);
			if (!f.exists()) {
				f.getParentFile().mkdirs();
				f.createNewFile();
			}

			Files.copy(multipartFile.getInputStream(), fileNamePath, StandardCopyOption.REPLACE_EXISTING);
			originalfileName = multipartFile.getOriginalFilename();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage(), e.getCause());
		}
		return originalfileName;
	}

	private void updateIvrInfoMusic(IVRInfo ivrInfo, Long ivrId, boolean isNewEntry) throws InvoTellServiceException {
		try {

			if (isNewEntry) {
				ivrInfoService.addNewIvrInfo(ivrId, ivrInfo);
			} else {
				ivrInfoService.updateIvrInfo(ivrId, ivrInfo.getKeyword(), ivrInfo);
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage(), e.getCause());
		}
	}
}
