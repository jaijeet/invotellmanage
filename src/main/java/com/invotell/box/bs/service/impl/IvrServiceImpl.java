package com.invotell.box.bs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invotell.box.bs.model.InvoTellError;
import com.invotell.box.bs.service.IVRService;
import com.invotell.box.bs.service.helper.IvrServiceHelper;
import com.invotell.box.comn.exception.InvoTellPersistanceException;
import com.invotell.box.comn.exception.InvoTellServiceException;
import com.invotell.box.comn.exception.InvoTellValidationException;
import com.invotell.box.dao.entity.IVR;
import com.invotell.box.dao.entity.IVRInfo;
import com.invotell.box.dao.entity.PbxIvrTree;
import com.invotell.box.dao.entity.Queue;
import com.invotell.box.dao.repository.IVRRepository;
import com.invotell.box.dao.repository.PbxIvrTreeRepository;
import com.invotell.box.dao.repository.QueueRepository;

@Service
public class IvrServiceImpl implements IVRService {

	private static final Logger LOGGER = LoggerFactory.getLogger(IvrServiceImpl.class);

	@Autowired
	private IvrServiceHelper ivrServiceHelper;
	
	@Autowired
	private IVRRepository ivrRepository;
		
	@Autowired
	private QueueRepository queueRepository;
	
	@Autowired
	private PbxIvrTreeRepository pbxIvrRepository;
	
	@Override
	public IVR addNewIvr(IVR ivrToSave) throws InvoTellServiceException, InvoTellValidationException {
		IVR result = null;
		try {
			if (ivrToSave == null) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Ivr details are required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			
			IVR existingIvr = ivrRepository.getIvrByIvrId(ivrToSave.getIvrID(),false,false);
			if(existingIvr!=null)
			{
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Ivr with ivrID ["+ivrToSave.getIvrID()+"] already exists");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			
			result = ivrRepository.saveIvr(ivrToSave);
			
			/*
			 * CODE TO CREATE THE FIRST DEFAULT QUEUE AND PBXIVRTREE
			 * */
			Queue defaultQueue = ivrServiceHelper.createDefaultQueue(ivrToSave.getIvrID());
			defaultQueue.setIvrId(result.getIvrID());
			PbxIvrTree defaultIvrTree =  ivrServiceHelper.createDefaultPbxIvrTree(defaultQueue);
			defaultIvrTree.setIvrid(result.getIvrID());
			queueRepository.saveQueue(defaultQueue);
			pbxIvrRepository.savePbxTree(defaultIvrTree);
			
		} catch (InvoTellValidationException itve) {
			throw itve;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}
		return result;
	}
	
	@Override
	public IVR getIvrById(Long ivrID,boolean ivrInfo, boolean queue) throws InvoTellServiceException, InvoTellValidationException {
		IVR result = null;
		boolean isFirstTimeCall=false;
		try {
			if (ivrID == null || ivrID==0L) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Enter valid IVR ID");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			result = ivrRepository.getIvrByIvrId(ivrID,ivrInfo,queue);
			if(result!=null && result.getIvrInfo().size()==0)
			{
				List<IVRInfo> infos = ivrServiceHelper.craeteIvrInfo(result.getIvrID());
				result.setIvrInfo(infos);
				isFirstTimeCall=true;
			}
			
			if(isFirstTimeCall) {
				ivrRepository.updateIvr(result,ivrID);
			}
			
		} catch (InvoTellPersistanceException ive) {
			LOGGER.error(ive.getMessage(), ive);
			throw new InvoTellServiceException(ive.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}

		return result;
	}
	
	@Override
	public void deleteNewIvr(Long ivrID) throws InvoTellServiceException, InvoTellValidationException {
		// TODO Auto-generated method stub
		try {
			if (ivrID == null || ivrID==0L) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Enter valid IVR ID");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			IVR result = ivrRepository.getIvrByIvrId(ivrID,false,false);
			if(result==null)
			{
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Ivr with ivrID ["+ivrID+"] not found");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			
			ivrRepository.deleteIvr(result);
		} catch (InvoTellPersistanceException ive) {
			LOGGER.error(ive.getMessage(), ive);
			throw new InvoTellServiceException(ive.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}
	}
	
	
	@Override
	public IVR updateIvr(Long ivrID, IVR ivrToUpdate) throws InvoTellServiceException, InvoTellValidationException {
		IVR result = null;
		try {
			if (ivrID == null || ivrID==0L) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Enter valid IVR ID");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			result = ivrRepository.getIvrByIvrId(ivrID,true,true);
			if(result==null)
			{
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Ivr with ivrID ["+ivrID+"] not found");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			copyIvrEntity(result, ivrToUpdate);
			ivrRepository.updateIvr(result,ivrID);
		} catch (InvoTellPersistanceException ive) {
			LOGGER.error(ive.getMessage(), ive);
			throw new InvoTellServiceException(ive.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}
		return result;
	}

	
	@Override
	public List<IVR> getAllIvrList() throws InvoTellServiceException, InvoTellValidationException
	{
		List<IVR> result=null;
		try {
			
			result = ivrRepository.getIvrList();
		} catch (InvoTellPersistanceException ive) {
			LOGGER.error(ive.getMessage(), ive);
			throw new InvoTellServiceException(ive.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}
		return result;
	}
	
	@Override
	public IVR getIvrByIdWithoutIvrInfo(Long ivrID, boolean ivrInfo, boolean queue)
			throws InvoTellServiceException, InvoTellValidationException {
		IVR result = null;
		try {
			if (ivrID == null || ivrID==0L) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Enter valid IVR ID");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			result = ivrRepository.getIvrByIvrId(ivrID,ivrInfo,queue);
			
		} catch (InvoTellPersistanceException ive) {
			LOGGER.error(ive.getMessage(), ive);
			throw new InvoTellServiceException(ive.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}

		return result;
	}
	
	public void copyIvrEntity(IVR existingIvr, IVR toUpdate)
	{
		existingIvr.setIsDeleted(toUpdate.getIsDeleted());
		existingIvr.setIvrName(toUpdate.getIvrName());
	}
	
}
