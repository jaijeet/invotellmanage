package com.invotell.box.bs.service.impl;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invotell.box.bs.model.InvoTellError;
import com.invotell.box.bs.service.UserService;

import com.invotell.box.comn.exception.InvoTellPersistanceException;
import com.invotell.box.comn.exception.InvoTellServiceException;
import com.invotell.box.comn.exception.InvoTellValidationException;
import com.invotell.box.comn.exception.NotFoundException;
import com.invotell.box.dao.entity.Skill;
import com.invotell.box.dao.entity.User;
import com.invotell.box.dao.entity.UserSkill;
import com.invotell.box.dao.repository.SkillRepository;
import com.invotell.box.dao.repository.UserRepository;
import com.invotell.box.dao.repository.UserSkillRepository;

@Service
public class UserServiceImpl implements UserService {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private SkillRepository skillRepository;

	
	@Autowired
	private UserSkillRepository userSkillRepository;
	
	@Override
	public User addNewUser(User user) throws InvoTellServiceException, InvoTellValidationException {
		User result = null;
		try {

			if (user == null) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "User details are required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			User existedUser = userRepository.findUserByEmail(user.getEmail(),true);
			if (existedUser != null) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST,
						"User with email [" + user.getEmail() + "] already exists");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			result = userRepository.saveUser(user);
		} catch (InvoTellValidationException itve) {
			throw itve;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}
		return result;
	}

	@Override
	public User getUserById(Long id) throws InvoTellServiceException {
		try {

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}
		return null;
	}

	@Override
	public User getUserByEmail(String email)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException {
		User result = null;
		try {
			if (email == null) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "User email is required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			result = userRepository.findUserByEmail(email,false);
			if (result == null) {
				NotFoundException validationException = new NotFoundException(
						"User with email [" + email + "] doesn't found");
				throw validationException;
			}

			// GET THE USER SKILL FROM USERID
			
			List<UserSkill> userSkillList = userSkillRepository.getUserSkillsByUserId(result.getUserId());
			result.setUserSkill(userSkillList);

		} catch (NotFoundException nfe) {
			LOGGER.error(nfe.getMessage(), nfe);
			throw nfe;
		} catch (InvoTellPersistanceException ive) {
			LOGGER.error(ive.getMessage(), ive);
			throw new InvoTellServiceException(ive.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}

		return result;
	}

	@Override
	public User updateUser(String email, User userToUpdate) throws InvoTellServiceException {
		User result = null;
		try {
			if (userToUpdate == null) {
				// validation exception should be there
			}

			User existingUser = userRepository.findUserByEmail(email,true);

			if (!userToUpdate.getEmail().equals(existingUser.getEmail())) {
				User validate = userRepository.findUserByEmail(userToUpdate.getEmail(),true);
				if (validate != null) {
					// validation exception should be there
				}
			}
			updateEntity(userToUpdate, existingUser);

			userRepository.saveUser(existingUser);

		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}

	@Override
	public List<User> getAllUsers() throws InvoTellServiceException, NotFoundException {
		List<User> userList = null;
		try {
			userList = userRepository.findAllUser();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}
		return userList;
	}

	@Override
	public void deleteUser(String email)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException {
		try {
			if (email == null) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Email is required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			User existingUser = userRepository.findUserByEmail(email,true);

			if (!email.equals(existingUser.getEmail())) {
				NotFoundException notFoundExcp = new NotFoundException("User with email [" + email + "] not found");
				throw notFoundExcp;
			}

			userRepository.saveUser(existingUser);

		} catch (InvoTellValidationException validationException) {
			throw validationException;
		} catch (NotFoundException notFoundExcp) {
			throw notFoundExcp;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e);
		}
	}

	private User updateEntity(User updatingUser, User existingUser) {
		existingUser.setEmail(updatingUser.getEmail());
		existingUser.setExtension(updatingUser.getExtension());
		existingUser.setFirstName(updatingUser.getFirstName());
		existingUser.setLastName(updatingUser.getLastName());
		existingUser.setPhone(updatingUser.getPhone());
		existingUser.setSipID(updatingUser.getSipID());
		return existingUser;
	}
}
