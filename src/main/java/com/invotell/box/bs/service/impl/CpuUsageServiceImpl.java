package com.invotell.box.bs.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.invotell.box.bs.model.CpuMemory;
import com.invotell.box.bs.model.Memory;
import com.invotell.box.bs.service.CpuUsageService;
import com.invotell.box.comn.exception.InvoTellServiceException;
import com.invotell.box.comn.exception.NotFoundException;
import com.invotell.box.dao.constant.CommonConstant;
import com.invotell.box.dao.entity.CPU;
import com.invotell.box.dao.repository.CpuRepository;

@Service
public class CpuUsageServiceImpl implements CpuUsageService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CpuUsageServiceImpl.class);

	@Autowired
	private CpuRepository cpuRepository;
	
	@Override
	public List<CPU> getAllCpuUsage() throws InvoTellServiceException {
		List<CPU> result = null;
		try {
			result = cpuRepository.getCpuUsageList(true);
			if(result==null || result.size()==0) {
				NotFoundException notFoundExcp = new NotFoundException("CPU usage is not found");
				throw notFoundExcp;
			}
		} catch (NotFoundException notFoundExcp) {
			throw new InvoTellServiceException(notFoundExcp);
		}catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e);
		}
		return result;
	}
	
	public CpuMemory getCpuMemoryUsage() throws InvoTellServiceException{
		CpuMemory result = null;
		try {
			String script = CommonConstant.SCRIPT_PATH+CommonConstant.MEMORY_SCRIPT;
			Process p = Runtime.getRuntime().exec(script);
			BufferedReader read = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuffer data = new StringBuffer();
            String line = "";
            while ((line = read.readLine()) != null) {
                data.append(line);
            }           
            
			Gson gson = new Gson();
			result = gson.fromJson(data.toString(), CpuMemory.class);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e);
		}
		return result;
	}
}
