package com.invotell.box.bs.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invotell.box.bs.model.InvoTellError;
import com.invotell.box.bs.service.QueueService;
import com.invotell.box.comn.exception.InvoTellServiceException;
import com.invotell.box.comn.exception.InvoTellValidationException;
import com.invotell.box.comn.exception.NotFoundException;
import com.invotell.box.dao.entity.IVR;
import com.invotell.box.dao.entity.Queue;
import com.invotell.box.dao.repository.QueueRepository;
import com.invotell.box.dao.repository.IVRRepository;

@Service
public class QueueServiceImpl implements QueueService {

	private static final Logger LOGGER = LoggerFactory.getLogger(IvrInfoServiceImpl.class);

	@Autowired
	private IVRRepository ivrRepository;

	@Autowired
	private QueueRepository queueRepository;

	@Override
	public Queue addNewQueue(Long ivrID, Queue queueToSave)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException {
		Queue queue = null;
		try {
			if (ivrID == null || ivrID == 0L) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "IVRID is required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			IVR existedIvr = ivrRepository.getIvrByIvrId(ivrID, false, true);
			if (existedIvr == null) {
				NotFoundException notFoundExcp = new NotFoundException("IVR with ivrID [" + ivrID + "] not found");
				throw notFoundExcp;
			}
			
			queueToSave.setIvrId(existedIvr.getIvrID());
			
			queueRepository.saveQueue(queueToSave);

		} catch (InvoTellValidationException validationException) {
			throw validationException;
		} catch (NotFoundException notFoundExcp) {
			throw notFoundExcp;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e);
		}
		return queue;
	}

	@Override
	public Queue getQueue(Long ivrID, Long queueID)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException {
		Queue info = null;
		try {

			if (ivrID == null || ivrID == 0L) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "IVRID is required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			IVR existedIvr = ivrRepository.getIvrByIvrId(ivrID, false, false);
			if (existedIvr == null) {
				NotFoundException notFoundExcp = new NotFoundException("IVR with ivrID [" + ivrID + "] not found");
				throw notFoundExcp;
			}

			info = queueRepository.getQueue(ivrID, queueID);

		} catch (InvoTellValidationException validationException) {
			throw validationException;
		} catch (NotFoundException notFoundExcp) {
			throw notFoundExcp;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e);
		}
		return info;
	}

	@Override
	public void deleteQueue(Long ivrID, Long queueId)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException {
		try {
			if (ivrID == null || ivrID == 0L) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "IVRID is required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			IVR existedIvr = ivrRepository.getIvrByIvrId(ivrID, false, false);
			if (existedIvr == null) {
				NotFoundException notFoundExcp = new NotFoundException("IVR with ivrID [" + ivrID + "] not found");
				throw notFoundExcp;
			}

			Queue queue = queueRepository.getQueue(existedIvr.getId(), queueId);
			if (queue == null) {
				NotFoundException notFoundExcp = new NotFoundException(
						"Queue with ivrID [" + ivrID + "] and queue [" + queueId + "] not found");
				throw notFoundExcp;
			}

			queueRepository.deleteQueue(queue);

		} catch (InvoTellValidationException validationException) {
			throw validationException;
		} catch (NotFoundException notFoundExcp) {
			throw notFoundExcp;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e);
		}
	}
}
