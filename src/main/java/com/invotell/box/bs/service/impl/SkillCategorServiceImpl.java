package com.invotell.box.bs.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invotell.box.bs.model.InvoTellError;
import com.invotell.box.bs.service.SkillCategoryService;
import com.invotell.box.comn.exception.InvoTellPersistanceException;
import com.invotell.box.comn.exception.InvoTellServiceException;
import com.invotell.box.comn.exception.InvoTellValidationException;
import com.invotell.box.dao.entity.SkillCategory;
import com.invotell.box.dao.repository.SkillCategoryRepository;

@Service
public class SkillCategorServiceImpl implements SkillCategoryService {
	private static final Logger LOGGER = LoggerFactory.getLogger(SkillCategorServiceImpl.class);
	
	@Autowired
	private SkillCategoryRepository skillCategorRepository;
	
	@Override
	public SkillCategory addNewSkillCategory(SkillCategory categoryToSave) throws InvoTellServiceException,InvoTellValidationException {
		SkillCategory result = null;
		try {
			if (categoryToSave == null) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Skill category details are required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			
			SkillCategory existingCat = skillCategorRepository.getSkillCategoryBySkillName(categoryToSave.getName());
			if(existingCat!=null)
			{
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Skill Category with skill name ["+categoryToSave.getName()+"] already exists");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			
			result = skillCategorRepository.addNewSkillCategory(categoryToSave);
			
		} catch (InvoTellValidationException itve) {
			throw itve;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}
		return result;
	}

	@Override
	public void updateSkillCategory(Long skillCategoryId, SkillCategory category) throws InvoTellServiceException, InvoTellValidationException {
		try {
			if (skillCategoryId == null || skillCategoryId==0L) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Enter valid Skill Category ID");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			if(category==null || category.getSkillCategoryID()==null || category.getSkillCategoryID()==0L) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Invalid Skill category data");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			
			SkillCategory result  = skillCategorRepository.getSkillCategoryBySkillId(skillCategoryId);
			if(result==null) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Skill Category with skill id ["+skillCategoryId+"] doesn't exist");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			
			skillCategorRepository.updateSkillCategory(category);
		}catch (InvoTellPersistanceException ive) {
			LOGGER.error(ive.getMessage(), ive);
			throw new InvoTellServiceException(ive.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}
	}

	@Override
	public SkillCategory getskillCategoryById(Long skillCategoryId) throws InvoTellServiceException, InvoTellValidationException {
		SkillCategory result = null;
		try {
			if (skillCategoryId == null || skillCategoryId==0L) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Enter valid Skill Category ID");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			result = skillCategorRepository.getSkillCategoryBySkillId(skillCategoryId);
						
		} catch (InvoTellPersistanceException ive) {
			LOGGER.error(ive.getMessage(), ive);
			throw new InvoTellServiceException(ive.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}

		return result;
	}

	@Override
	public SkillCategory getskillCategoryByName(String name) throws InvoTellServiceException, InvoTellValidationException {
		SkillCategory result = null;
		try {
			if (name == null || name.trim()=="") {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Enter valid Skill Category Name");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			result = skillCategorRepository.getSkillCategoryBySkillName(name);
						
		} catch (InvoTellPersistanceException ive) {
			LOGGER.error(ive.getMessage(), ive);
			throw new InvoTellServiceException(ive.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}
		return result;
	}

	@Override
	public List<SkillCategory> getAllSkillCategory(boolean loadChild) throws InvoTellServiceException, InvoTellValidationException {
		List<SkillCategory> result=null;
		try {
			
			result = skillCategorRepository.getSkillCategoryList(loadChild);
		} catch (InvoTellPersistanceException ive) {
			LOGGER.error(ive.getMessage(), ive);
			throw new InvoTellServiceException(ive.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}
		return result;
	}
	
	@Override
	public void deleteSkillCategory(Long catId, String name) throws InvoTellServiceException, InvoTellValidationException {
		try {
			if ( (catId == null || catId==0L) && (name == null || name.equals(""))) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Enter valid Category Id and name");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			
			SkillCategory check = null;
			
			if(catId!=null && catId>0L ) {
				check = skillCategorRepository.getSkillCategoryBySkillId(catId);
			}else {
				check  = skillCategorRepository.getSkillCategoryBySkillName(name);
			}
			
			if(check==null) {
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Skill category with skill id and name is not found");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			
			skillCategorRepository.deleteSkillCategory(catId);
	
		} catch (InvoTellPersistanceException ive) {
			LOGGER.error(ive.getMessage(), ive);
			throw new InvoTellServiceException(ive.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}
	}

}
