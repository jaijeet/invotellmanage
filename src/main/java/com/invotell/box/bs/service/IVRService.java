package com.invotell.box.bs.service;

import java.util.List;

import com.invotell.box.comn.exception.InvoTellServiceException;
import com.invotell.box.comn.exception.InvoTellValidationException;
import com.invotell.box.dao.entity.IVR;

public interface IVRService {
	IVR addNewIvr (IVR ivr) throws InvoTellServiceException,InvoTellValidationException;
	IVR getIvrById(Long id,boolean ivrInfo, boolean queue ) throws InvoTellServiceException,InvoTellValidationException;
	void deleteNewIvr (Long ivrId) throws InvoTellServiceException,InvoTellValidationException;
	IVR updateIvr(Long ivrID,IVR ivr) throws InvoTellServiceException,InvoTellValidationException;
	List<IVR> getAllIvrList() throws InvoTellServiceException,InvoTellValidationException;
	IVR getIvrByIdWithoutIvrInfo(Long id,boolean ivrInfo, boolean queue ) throws InvoTellServiceException,InvoTellValidationException;
}
