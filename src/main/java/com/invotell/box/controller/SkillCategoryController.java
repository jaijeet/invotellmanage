package com.invotell.box.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.invotell.box.bs.model.RequestInfo;
import com.invotell.box.bs.model.ResponseInfo;
import com.invotell.box.bs.service.SkillCategoryService;
import com.invotell.box.comn.exception.InvoTellException;
import com.invotell.box.dao.entity.SkillCategory;
import com.invotell.box.dao.modal.MessageInfo;


@RestController
@RequestMapping(value = "/v1/manage/skill/")
public class SkillCategoryController {

	@Autowired
	private SkillCategoryService skillCategoryService;
	
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, path = "category/add")
	public ResponseEntity<ResponseInfo<MessageInfo>> addNewSkillCategory(@RequestBody RequestInfo<SkillCategory> requestInfo) throws InvoTellException {
		MessageInfo response = new MessageInfo();
		try {
			SkillCategory skillCatToAdd = requestInfo.getRequestInfo();
			skillCategoryService.addNewSkillCategory(skillCatToAdd);
			response.setCode(HttpStatus.CREATED.value());
			response.setMessage("Skill Category is created");
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<MessageInfo>>(new ResponseInfo<MessageInfo>(response), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, path = "category/id")
	public ResponseEntity<ResponseInfo<SkillCategory>> getSkillCategoryById(@RequestParam("catId") Long catId) throws InvoTellException {
		SkillCategory response = null;
		try {
			response = skillCategoryService.getskillCategoryById(catId);
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<SkillCategory>>(new ResponseInfo<SkillCategory>(response), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, path = "category/name")
	public ResponseEntity<ResponseInfo<SkillCategory>> getSkillCategoryByName(@RequestParam("name") String name) throws InvoTellException {
		SkillCategory response = null;
		try {
			response = skillCategoryService.getskillCategoryByName(name);
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<SkillCategory>>(new ResponseInfo<SkillCategory>(response), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, path = "category/update")
	public ResponseEntity<ResponseInfo<MessageInfo>> updateSkillCategory(@RequestParam("skillCatId") Long catId, @RequestBody RequestInfo<SkillCategory> requestInfo) throws InvoTellException {
		MessageInfo response = new MessageInfo();
		try {
			SkillCategory entity = requestInfo.getRequestInfo();
			skillCategoryService.updateSkillCategory(catId, entity);
			response.setCode(HttpStatus.OK.value());
			response.setMessage("Skill Category is updated");
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<MessageInfo>>(new ResponseInfo<MessageInfo>(response), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, path = "category/list")
	public ResponseEntity<ResponseInfo<List<SkillCategory>>> getSkillCategoryList() throws InvoTellException {
		List<SkillCategory> response = null;
		try {
			response = skillCategoryService.getAllSkillCategory(false);
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<List<SkillCategory>>>(new ResponseInfo<List<SkillCategory>>(response), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE, path = "category/delete")
	public ResponseEntity<ResponseInfo<MessageInfo>> deleteSkillCategory(@RequestParam("skillCatId") Long catId, @RequestParam("name") String name) throws InvoTellException {
		MessageInfo response = new MessageInfo();
		try {
			skillCategoryService.deleteSkillCategory(catId, name);
			response.setCode(HttpStatus.OK.value());
			response.setMessage("Skill Category is updated");
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<MessageInfo>>(new ResponseInfo<MessageInfo>(response), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, path = "category/list/skills")
	public ResponseEntity<ResponseInfo<List<SkillCategory>>> getSkillCategoryListWithSkills() throws InvoTellException {
		List<SkillCategory> response = null;
		try {
			response = skillCategoryService.getAllSkillCategory(true);
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<List<SkillCategory>>>(new ResponseInfo<List<SkillCategory>>(response), HttpStatus.OK);
	}
	
}
