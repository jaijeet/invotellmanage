package com.invotell.box.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.invotell.box.bs.model.ResponseInfo;
import com.invotell.box.bs.service.UploadService;
import com.invotell.box.comn.exception.InvoTellException;
import com.invotell.box.dao.modal.MessageInfo;


@RestController
@RequestMapping(value = "/v1/manage/upload")
public class UploadController {

	@Autowired
	private UploadService uploadService;

	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, path = "/ivr/{ivrID}/{keyword}")
	public ResponseEntity<ResponseInfo<MessageInfo>> uploadToLocalFileSystem(
			@RequestParam("file") MultipartFile multipartFile, @PathVariable("ivrID") Long ivrID,
			@PathVariable("keyword") String keyword) throws InvoTellException {
		MessageInfo response = new MessageInfo();
		System.out.println(multipartFile);
		try {
			String msg = uploadService.uploadIvrMusicFiles(multipartFile, ivrID, keyword);
			response.setMessage(msg);
			response.setCode(HttpStatus.OK.value());
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<MessageInfo>>(new ResponseInfo<MessageInfo>(response),
				HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, path = "/queue/{ivrID}/{queueID}")
	public ResponseEntity<ResponseInfo<MessageInfo>> uploadQueueLocalFileSystem(
			@RequestParam("file") MultipartFile multipartFile, @PathVariable("ivrID") Long ivrID,
			@PathVariable("queueID") Long qid) throws InvoTellException {
		MessageInfo response = new MessageInfo();
		try {
			String msg = uploadService.uploadQueuePromptFile(multipartFile, ivrID, qid);
			response.setMessage(msg);
			response.setCode(HttpStatus.OK.value());
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<MessageInfo>>(new ResponseInfo<MessageInfo>(response),
				HttpStatus.CREATED);
	}

}
