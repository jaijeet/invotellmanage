package com.invotell.box.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.invotell.box.bs.model.RequestInfo;
import com.invotell.box.bs.model.ResponseInfo;
import com.invotell.box.bs.service.IVRInfoService;
import com.invotell.box.bs.service.IVRService;
import com.invotell.box.bs.service.QueueService;
import com.invotell.box.comn.exception.InvoTellException;
import com.invotell.box.dao.entity.IVR;
import com.invotell.box.dao.entity.IVRInfo;
import com.invotell.box.dao.entity.Queue;
import com.invotell.box.dao.modal.MessageInfo;

@RestController
@RequestMapping(value = "/v1/manage/ivr/")
public class IvrController {

	@Autowired
	private IVRService ivrService;	
	
	
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE,path="add")
	public ResponseEntity<ResponseInfo<MessageInfo>> addNewIvr(@RequestBody RequestInfo<IVR> requestInfo)
			throws InvoTellException {
		MessageInfo response = new MessageInfo();
		try {
			IVR entity = requestInfo.getRequestInfo();
			if (entity != null) {
				ivrService.addNewIvr(entity);
				String msg = "Ivr is added";
				response.setCode(HttpStatus.CREATED.value());
				response.setMessage(msg);
			}
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<MessageInfo>>(new ResponseInfo<MessageInfo>(response), HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, path="single")
	public ResponseEntity<ResponseInfo<IVR>> getExistingIvr(@RequestParam("ivrID") Long ivrID)
			throws InvoTellException {
		IVR response = null;
		try {
			response = ivrService.getIvrById(ivrID,true,true);
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<IVR>>(new ResponseInfo<IVR>(response), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE, path="delete")
	public ResponseEntity<ResponseInfo<MessageInfo>> deleteExistingIvr(@RequestParam("ivrID") Long ivrID)
			throws InvoTellException {
		MessageInfo response = null;
		try {
			ivrService.deleteNewIvr(ivrID);
			String msg="Ivr with ivrId ["+ivrID+"] successfully deleted";
			response.setCode(HttpStatus.OK.value());
			response.setMessage(msg);
			
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<MessageInfo>>(new ResponseInfo<MessageInfo>(response), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE,path="update")
	public ResponseEntity<ResponseInfo<MessageInfo>> updateNewIvr(@RequestBody RequestInfo<IVR> requestInfo, @RequestParam("ivrID") Long ivrID)
			throws InvoTellException {
		MessageInfo response = null;
		try {
			IVR entity = requestInfo.getRequestInfo();
			if (entity != null) {
				ivrService.updateIvr(ivrID, entity);
				response.setCode(HttpStatus.OK.value());
				response.setMessage("Ivr is updated");
			}
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<MessageInfo>>(new ResponseInfo<MessageInfo>(response), HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, path = "all")
	public ResponseEntity<ResponseInfo<List<IVR>>> getAllExistingIvr()
			throws InvoTellException {
		List<IVR> response = null;
		try {
			response = ivrService.getAllIvrList();
			
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<List<IVR>>>(new ResponseInfo<List<IVR>>(response), HttpStatus.OK);
	}

}
