package com.invotell.box.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.invotell.box.bs.model.CpuMemory;
import com.invotell.box.bs.model.ResponseInfo;
import com.invotell.box.bs.service.CpuUsageService;
import com.invotell.box.comn.exception.InvoTellException;
import com.invotell.box.dao.entity.CPU;

@RestController
@RequestMapping(value = "/v1/dashboad/chart/")
public class DashboardChartController {
	
	@Autowired
	private CpuUsageService cpuUsageService;
	
	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, path = "cpu")
	public ResponseEntity<ResponseInfo<List<CPU>>> getCpuChartData()
			throws InvoTellException {
		List<CPU> response = null;
		try {
			response = cpuUsageService.getAllCpuUsage();
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<List<CPU>>>(new ResponseInfo<List<CPU>>(response),HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, path = "memory")
	public ResponseEntity<ResponseInfo<CpuMemory>> getCpuMemoryData() throws InvoTellException{
		CpuMemory response = null;
		try {
			response = cpuUsageService.getCpuMemoryUsage();
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<CpuMemory>>(new ResponseInfo<CpuMemory>(response),HttpStatus.OK);
	}
}
