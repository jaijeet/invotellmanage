package com.invotell.box.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.invotell.box.bs.model.RequestInfo;
import com.invotell.box.bs.model.ResponseInfo;
import com.invotell.box.bs.service.UserService;
import com.invotell.box.comn.exception.InvoTellException;
import com.invotell.box.dao.entity.User;


@RestController
@RequestMapping(value = "/v1/manage/user")
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseInfo<String>> addNewUser(@RequestBody RequestInfo<User> requestInfo)
			throws InvoTellException {
		String response = null;
		try {
			User entity = requestInfo.getRequestInfo();
			if(entity!=null)
			{
				userService.addNewUser(entity);
				response = "User is added";
			}
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<String>>(new ResponseInfo<String>(response),HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseInfo<User>> getUserByEmail(@RequestParam("email") String email)
			throws InvoTellException {
		User result = null;
		try {
			result = userService.getUserByEmail(email);

		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}

		return new ResponseEntity<ResponseInfo<User>>(new ResponseInfo<User>(result), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, path = "all")
	public ResponseEntity<ResponseInfo<List<User>>> getAllUsers()
			throws InvoTellException {
		List<User> result = null;
		try {
			result = userService.getAllUsers();

		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}

		return new ResponseEntity<ResponseInfo<List<User>>>(new ResponseInfo<List<User>>(result), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseInfo<String>> updateUser(@RequestParam("email") String email,@RequestBody RequestInfo<User> requestInfo)
			throws InvoTellException {
		String result = null;
		try {
			User entity = requestInfo.getRequestInfo();
			userService.updateUser(email, entity);
			result = "User is updated";
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}

		return new ResponseEntity<ResponseInfo<String>>(new ResponseInfo<String>(result), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseInfo<String>> deleteUserByEmail(@RequestParam("email") String email)
			throws InvoTellException {
		String result = null;
		try {
			 userService.deleteUser(email);
			 result = "User is successfully deleted";
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}

		return new ResponseEntity<ResponseInfo<String>>(new ResponseInfo<String>(result), HttpStatus.OK);
	}
}
