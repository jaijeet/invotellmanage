package com.invotell.box.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.invotell.box.bs.model.RequestInfo;
import com.invotell.box.bs.model.ResponseInfo;
import com.invotell.box.bs.service.QueueInfoService;
import com.invotell.box.comn.exception.InvoTellException;
import com.invotell.box.dao.entity.QueueInfo;
import com.invotell.box.dao.modal.MessageInfo;

@RestController
@RequestMapping(value = "/v1/manage/queueinfo/")
public class QueueInfoController {
	
	@Autowired
	private QueueInfoService queueInfoService;

	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, path = "add/multiple/{ivrID}/{queueID}")
	public ResponseEntity<ResponseInfo<MessageInfo>> addQueueInfoList(@RequestBody RequestInfo<List<QueueInfo>> requestInfo,@PathVariable("ivrID") Long ivrId,@PathVariable("queueID") Long queueId) throws InvoTellException {
		MessageInfo response = new MessageInfo();
		try {
			List<QueueInfo> listToAdd = requestInfo.getRequestInfo();
			queueInfoService.addQueueInfoList(ivrId, queueId, listToAdd);
			response.setCode(HttpStatus.CREATED.value());
			response.setMessage("Queue info list is created");
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<MessageInfo>>(new ResponseInfo<MessageInfo>(response), HttpStatus.OK);
	}

}