package com.invotell.box.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.invotell.box.bs.model.PbxQueue;
import com.invotell.box.bs.model.RequestInfo;
import com.invotell.box.bs.model.ResponseInfo;
import com.invotell.box.bs.service.PbxTreeService;
import com.invotell.box.bs.service.QueueService;
import com.invotell.box.bs.service.helper.IvrServiceHelper;
import com.invotell.box.comn.exception.InvoTellException;
import com.invotell.box.dao.entity.PbxIvrTree;
import com.invotell.box.dao.entity.Queue;
import com.invotell.box.dao.modal.MessageInfo;


@RestController
@RequestMapping(value = "/v1/manage/queue/")
public class QueueController {

	@Autowired
	private QueueService queueService;

	@Autowired
	private PbxTreeService pbxIvrService;

	@Autowired
	private IvrServiceHelper helper;

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, path = "addqueue/{parentNode}")
	public ResponseEntity<ResponseInfo<MessageInfo>> addNewQueue(@RequestParam("ivrID") Long ivrID,
			@RequestBody RequestInfo<Queue> requestInfo, @PathVariable("parentNode") Integer parentNode)
			throws InvoTellException {
		MessageInfo response = new MessageInfo();
		try {
			Queue entity = requestInfo.getRequestInfo();
			if (entity != null) {
				PbxIvrTree pbxIvrTree = helper.createPbxIvrTreeByQueue(entity, parentNode, false, false);
				pbxIvrTree.setIvrid(ivrID);
				queueService.addNewQueue(ivrID, entity);
				pbxIvrService.addPbxIvrTree(pbxIvrTree);
				response.setCode(HttpStatus.CREATED.value());
				response.setMessage("Queue is added");
			}
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<MessageInfo>>(new ResponseInfo<MessageInfo>(response),
				HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, path = "single")
	public ResponseEntity<ResponseInfo<Queue>> getQueue(@RequestParam("ivrID") Long ivrID,
			@RequestParam("queueID") Long queueID) throws InvoTellException {
		Queue response = null;
		try {
			response = queueService.getQueue(ivrID, queueID);
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<Queue>>(new ResponseInfo<Queue>(response), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE, path = "delete")
	public ResponseEntity<ResponseInfo<MessageInfo>> deleteQueue(@RequestParam("ivrID") Long ivrID,
			@RequestParam("queueID") Long queueID) throws InvoTellException {
		MessageInfo response = new MessageInfo();
		try {
			queueService.deleteQueue(ivrID, queueID);
			response.setCode(HttpStatus.OK.value());
			response.setMessage("Queue [" + queueID + "] successfully deleted");
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<MessageInfo>>(new ResponseInfo<MessageInfo>(response), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, path = "addprompt/{parentNode}/{status}")
	public ResponseEntity<ResponseInfo<MessageInfo>> addNewPrompt(@RequestParam("ivrID") Long ivrID,
			@RequestBody RequestInfo<Queue> requestInfo, @PathVariable("parentNode") Integer parentNode,
			@PathVariable("status") boolean promptStatus, @RequestParam("file") String fileName)
			throws InvoTellException {
		MessageInfo response = new MessageInfo();
		try {
			Queue entity = requestInfo.getRequestInfo();
			if (entity != null) {
				PbxIvrTree pbxIvrTree = helper.createPbxIvrTreeByQueue(entity, parentNode, true, promptStatus);
				pbxIvrTree.setIvrid(ivrID);
				pbxIvrTree.setPromtFile(fileName);
				pbxIvrService.addPbxIvrTree(pbxIvrTree);
				response.setCode(HttpStatus.CREATED.value());
				response.setMessage("Interim voice prompt is added");
			}
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<MessageInfo>>(new ResponseInfo<MessageInfo>(response),
				HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, path = "ivrtree")
	public ResponseEntity<ResponseInfo<List<PbxQueue>>> getPbxTreeQueue(@RequestParam("ivrID") Long ivrID)
			throws InvoTellException {
		List<PbxQueue> response = null;
		try {
			response = pbxIvrService.getPbxTreeByIvrId(ivrID);
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<List<PbxQueue>>>(new ResponseInfo<List<PbxQueue>>(response),
				HttpStatus.OK);
	}
}
