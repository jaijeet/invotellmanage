package com.invotell.box.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.invotell.box.bs.model.RequestInfo;
import com.invotell.box.bs.model.ResponseInfo;
import com.invotell.box.bs.service.IVRInfoService;
import com.invotell.box.comn.exception.InvoTellException;
import com.invotell.box.dao.entity.IVRInfo;
import com.invotell.box.dao.modal.MessageInfo;


@RestController
@RequestMapping(value = "/v1/manage/ivrinfo/")
public class IvrInfoController {
	
	@Autowired
	private IVRInfoService ivrInfoService;
	
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, path = "add")
	public ResponseEntity<ResponseInfo<MessageInfo>> addNewIvrInfo(@RequestParam("ivrID") Long ivrID,
			@RequestBody RequestInfo<IVRInfo> requestInfo) throws InvoTellException {
		MessageInfo response = new MessageInfo();
		try {
			IVRInfo entity = requestInfo.getRequestInfo();
			if (entity != null) {
				ivrInfoService.addNewIvrInfo(ivrID, entity);
				response.setCode(HttpStatus.CREATED.value());
				response.setMessage("IvrInfo is added");
			}
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<MessageInfo>>(new ResponseInfo<MessageInfo>(response), HttpStatus.CREATED);
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, path = "single")
	public ResponseEntity<ResponseInfo<IVRInfo>> getIvrInfo(@RequestParam("ivrID") Long ivrID,@RequestParam("keyword") String keyword) throws InvoTellException {
		IVRInfo response = null;
		try {
			response = ivrInfoService.getIvrInfo(ivrID, keyword);
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<IVRInfo>>(new ResponseInfo<IVRInfo>(response), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, path = "update/single")
	public ResponseEntity<ResponseInfo<MessageInfo>> updateIvrInfo(@RequestParam("ivrID") Long ivrID,@RequestParam("keyword") String keyword, @RequestBody RequestInfo<IVRInfo> requestInfo) throws InvoTellException {
		MessageInfo response = new MessageInfo();
		try {
			IVRInfo entity = requestInfo.getRequestInfo();
			if (entity != null) {
				ivrInfoService.updateIvrInfo(ivrID, keyword,entity);
				response.setCode(HttpStatus.OK.value()); 
				response.setMessage("IvrInfo is updated");
			}
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<MessageInfo>>(new ResponseInfo<MessageInfo>(response), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, path = "update/multiple")
	public ResponseEntity<ResponseInfo<MessageInfo>> updateIvrInfoList(@RequestParam("ivrID") Long ivrID, @RequestBody RequestInfo<List<IVRInfo>> requestInfo) throws InvoTellException {
		MessageInfo response = new MessageInfo();
		try {
			List<IVRInfo> entity = requestInfo.getRequestInfo();
			if (entity != null) {
				ivrInfoService.updateIvrInfoList(ivrID, entity);
				response.setCode(HttpStatus.OK.value());
				response.setMessage("IvrInfo list is updated");
			}
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<MessageInfo>>(new ResponseInfo<MessageInfo>(response), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE, path = "delete")
	public ResponseEntity<ResponseInfo<MessageInfo>> deleteIvrInfo(@RequestParam("ivrID") Long ivrID,@RequestParam("keyword") String keyword) throws InvoTellException {
		MessageInfo response = new MessageInfo();
		try {
			ivrInfoService.deleteIvrInfo(ivrID, keyword);
			response.setCode(HttpStatus.OK.value());
			response.setMessage("Ivrinfo ["+keyword+"] successfully deleted");
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<MessageInfo>>(new ResponseInfo<MessageInfo>(response), HttpStatus.OK);
	}
	
}
