package com.invotell.box.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.invotell.box.bs.model.RequestInfo;
import com.invotell.box.bs.model.ResponseInfo;
import com.invotell.box.bs.service.SkillService;
import com.invotell.box.comn.exception.InvoTellException;
import com.invotell.box.dao.entity.Skill;
import com.invotell.box.dao.modal.MessageInfo;
import com.invotell.box.dao.modal.SkillModel;


@RestController
@RequestMapping(value = "/v1/manage/skill/")
public class SkillController {

	@Autowired
	private SkillService skillService;
	
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, path = "add")
	public ResponseEntity<ResponseInfo<MessageInfo>> addNewSkill(@RequestBody RequestInfo<Skill> requestInfo,@RequestParam("catId") Long categoryId) throws InvoTellException {
		MessageInfo response = new MessageInfo();
		try {
			Skill skillToAdd = requestInfo.getRequestInfo();
			skillService.addNewSkill(skillToAdd, categoryId);
			response.setCode(HttpStatus.CREATED.value());
			response.setMessage("Skill is created");
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<MessageInfo>>(new ResponseInfo<MessageInfo>(response), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, path = "single")
	public ResponseEntity<ResponseInfo<Skill>> getExistingSkillById(@RequestParam("skillId") Long skillId) throws InvoTellException {
		Skill response = null;
		try {
			response = skillService.getExistingSkillById(skillId);
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<Skill>>(new ResponseInfo<Skill>(response), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, path = "update")
	public ResponseEntity<ResponseInfo<Skill>> updateExistingSkill(@RequestBody RequestInfo<Skill> requestInfo,@RequestParam("skillId") Long skillId,@RequestParam("catId") Long skillcategoryid) throws InvoTellException {
		Skill response = null;
		try {
			Skill skillToUpdate = requestInfo.getRequestInfo();
			response = skillService.updateExistingSkill(skillToUpdate, skillId,skillcategoryid);
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<Skill>>(new ResponseInfo<Skill>(response), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE, path = "delete")
	public ResponseEntity<ResponseInfo<MessageInfo>> deleteExistingSkill(@RequestParam("skillId") Long skillId) throws InvoTellException {
		MessageInfo response = new MessageInfo();
		try {
			skillService.deleteExistingSkill(skillId);
			response.setCode(HttpStatus.CREATED.value());
			response.setMessage("Skill is successfully deleted");
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<MessageInfo>>(new ResponseInfo<MessageInfo>(response), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, path = "list")
	public ResponseEntity<ResponseInfo<List<Skill>>> getSkillByCategoryId(@RequestParam("catId") Long catId) throws InvoTellException {
		List<Skill> response = null;
		try {
			response = skillService.getSkillByCategoryId(catId);
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<List<Skill>>>(new ResponseInfo<List<Skill>>(response), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, path = "list/all")
	public ResponseEntity<ResponseInfo<List<SkillModel>>> getSkillByCategoryList() throws InvoTellException {
		List<SkillModel> response = null;
		try {
			response = skillService.getSkillList();
		} catch (Exception e) {
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<List<SkillModel>>>(new ResponseInfo<List<SkillModel>>(response), HttpStatus.OK);
	}
}
