package com.invotell.box.dao.constant;

public interface UploadFileConstant {
	public static final String UPLOAD_IVR_MUSIC_DIR = "/home/jaijeet/Documents/uploadDir/uploads/ivr/music/";
//	public static final String UPLOAD_IVR_MUSIC_DIR = "D:/Softwares/Company Projects/uploads/ivr/music/";
	
	public static final String UPLOAD_IVR_FILTER_DIR = "/home/jaijeet/Documents/uploadDir/uploads/ivr/filter/";
//	public static final String UPLOAD_IVR_FILTER_DIR = "D:/Softwares/Company Projects/uploads/ivr/filter/";
	
	public static final String UPLOAD_QUEUE_PROMPT_DIR = "/home/jaijeet/Documents/uploadDir/uploads/queue/music/";
//	public static final String UPLOAD_IVR_MUSIC_DIR = "D:/Softwares/Company Projects/uploads/queue/music/";
	
	public static final String WELCOME_FILE_NAME = "IvrWelcome";
	public static final String HOLD_FILE_NAME = "musicHold";
	public static final String NIGHTMODE_WELCOME_FILE_NAME = "nighModeWelcome";
	public static final String QUEUE_PROMPT = "queuePrompt";
}
