package com.invotell.box.dao.constant;

public enum IvrConstantEnum {
	ENABLE_IVR("enableIVR","off"), INBOUND_NUMBER("inbound",""), WELCOME_FILE("welcomeFile",""), MUSIC_ON_HOLD("onHoldMusic",""),
	EXTENSION_DIALING("extensionDial",""), START_TIME("startTime",""), START_DAY("startDay",""), END_TIME("endTime",""),
	END_DAY("endDay",""), DESTINATION_MATCH("destinationMatch","voicemail"), DESTINATION_NOT_MATCH("destinationNotMatch","voicemail"),
	DAY_OFF("holiday",""), NIGHT_MODE_DESTINATION("nightmodeDestination","voicemail"), NIGHT_MODE_WELCOME("nightmodeWelcome","");

	private String value;
	private String defaultVal;

	private IvrConstantEnum(String value,String defaultVal) {
		this.value = value;
		this.defaultVal = defaultVal;
	}

	public String getValue() {
		return value;
	}
	
	public String getDefaultVal() {
		return defaultVal;
	}
}
