package com.invotell.box.dao.constant;

public interface PersistanceConstant {
	public static final String USER_TABLE = "user";
	public static final String SKILL_TABLE = "skill";
	public static final String SKILL_CATEGORY_TABLE = "skillcategory";
	public static final String IVR_TABLE = "ivr";
	public static final String IVR_INFO_TABLE = "ivrinfo";
	public static final String QUEUE_TABLE = "queue";
	public static final String QUEUE_INFO_TABLE = "queueinfo";
	public static final String PBX_IVR_TREE_TABLE = "pbxivrtree";
	public static final String DOCUMNET_TABLE = "document";
	public static final String CPU_USAGE = "usagecputable";
	public static final String USER_SKILL = "userskill";
}
