package com.invotell.box.dao.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.invotell.box.comn.exception.InvoTellPersistanceException;
import com.invotell.box.dao.entity.CPU;
import com.invotell.box.dao.entity.QCPU;
import com.invotell.box.dao.repository.jpa.CPUJpaRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Repository
public class CpuRepository {
	private static final Logger LOGGER = LoggerFactory.getLogger(CpuRepository.class);

	@Autowired
	private CPUJpaRepository cpuJpaRepository;

	@PersistenceContext
	private EntityManager entityManager;

	@Transactional(readOnly = true)
	public List<CPU> getCpuUsageList(boolean clearSession) throws InvoTellPersistanceException {
		LOGGER.info("Inside the CpuRepository->getCpuUsageList()");
		List<CPU> result = new ArrayList<CPU>();
		try {

			QCPU cpu = QCPU.cPU;
			JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
			result = queryFactory.selectFrom(cpu).orderBy(cpu.time.asc()).fetch();
			if(clearSession) {
				entityManager.clear();
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return result;
	}
}
