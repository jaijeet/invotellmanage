package com.invotell.box.dao.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.invotell.box.comn.exception.InvoTellPersistanceException;
import com.invotell.box.dao.entity.PbxIvrTree;
import com.invotell.box.dao.entity.QPbxIvrTree;
import com.invotell.box.dao.repository.jpa.PbxIvrTreeJpaRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Repository
public class PbxIvrTreeRepository {
	private static final Logger LOGGER = LoggerFactory.getLogger(PbxIvrTreeRepository.class);
	
	@Autowired
	public PbxIvrTreeJpaRepository pbxIvrTreeJpaRepository;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	@Transactional(noRollbackFor = InvoTellPersistanceException.class)
	public PbxIvrTree savePbxTree(PbxIvrTree pbxIvrTree) throws InvoTellPersistanceException {
		LOGGER.info("Inside the PbxIvrTreeRepository->savePbxTree(PbxIvrTree pbxIvrTree)");
		PbxIvrTree ivr = null;
		try {
			ivr = pbxIvrTreeJpaRepository.save(pbxIvrTree);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return ivr;
	}
	
	@Transactional(readOnly = true)
	public Integer getMaxNodeId(Long ivrId) throws InvoTellPersistanceException {
		Integer nodeId=null;
		try {
			nodeId = pbxIvrTreeJpaRepository.getMaxNodeIdByIvrId(ivrId);
			entityManager.clear();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return nodeId;
	}
	
	@Transactional(readOnly = true)
	public List<PbxIvrTree> getPbxIvrTreeList(Long ivrId) throws InvoTellPersistanceException {
		List<PbxIvrTree> result=new ArrayList<PbxIvrTree>();
		try {
			
			JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
			QPbxIvrTree pbx = QPbxIvrTree.pbxIvrTree;
			result = queryFactory.selectFrom(pbx)
			.where(pbx.ivrid.eq(ivrId))
			  .orderBy(pbx.parenetNode.asc(),pbx.keyPress.asc())
			  .fetch();
			entityManager.clear();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return result;
	}
	
}
