package com.invotell.box.dao.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.invotell.box.comn.exception.InvoTellPersistanceException;
import com.invotell.box.dao.entity.QSkill;
import com.invotell.box.dao.entity.QUserSkill;
import com.invotell.box.dao.entity.User;
import com.invotell.box.dao.entity.UserSkill;
import com.invotell.box.dao.entity.Skill;
import com.invotell.box.dao.repository.jpa.SkillJpaRepository;
import com.invotell.box.dao.repository.jpa.UserSkillJpaRepository;

@Repository
public class UserSkillRepository {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserSkillRepository.class);

	@Autowired
	private UserSkillJpaRepository userSkillJpaRepository;
	
	@Autowired 
	private SkillJpaRepository skillJpaRepository;

	@PersistenceContext
	private EntityManager entityManager;

	public List<UserSkill> getUserSkillsByUserId(String userid) throws InvoTellPersistanceException {
		List<UserSkill> result = new ArrayList<UserSkill>();
		try {
			Iterable<UserSkill> iterable = userSkillJpaRepository.findAll(QUserSkill.userSkill.userId.eq(userid));
			iterable.forEach(userskill -> {
				Long skillId = userskill.getSkillId();
				
				Optional<Skill> optional = skillJpaRepository.findOne(QSkill.skill.skillId.eq(skillId));
				if(optional.isPresent()) {
					userskill.setSkill(optional.get());
				}
				
				result.add(userskill);
			});
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return result;
	}
}
