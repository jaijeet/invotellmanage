package com.invotell.box.dao.repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.invotell.box.comn.exception.InvoTellPersistanceException;
import com.invotell.box.dao.entity.QSkill;
import com.invotell.box.dao.entity.QSkillCategory;
import com.invotell.box.dao.entity.Skill;
import com.invotell.box.dao.entity.SkillCategory;
import com.invotell.box.dao.repository.jpa.SkillCategoryJpaRepository;
import com.invotell.box.dao.repository.jpa.SkillJpaRepository;

@Repository
public class SkillCategoryRepository {
	private static final Logger LOGGER = LoggerFactory.getLogger(SkillCategoryRepository.class);

	@Autowired
	private SkillCategoryJpaRepository skillCategoryJpaRepository;

	@Autowired
	private SkillJpaRepository skillJpaRepository;

	@PersistenceContext
	private EntityManager entityManager;

	@Transactional(readOnly = true)
	public Integer getMaxSkillId(Long ivrId) throws InvoTellPersistanceException {
		Integer nodeId = null;
		try {
			nodeId = skillCategoryJpaRepository.getMaxSkillCategoryId();
			entityManager.clear();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return nodeId;
	}

	@Transactional(noRollbackFor = InvoTellPersistanceException.class)
	public SkillCategory addNewSkillCategory(SkillCategory skillCategory) throws InvoTellPersistanceException {
		LOGGER.info("Inside the SkillCategoryRepository->savePbxTree(PbxIvrTree pbxIvrTree)");
		SkillCategory result = null;
		try {
			result = skillCategoryJpaRepository.save(skillCategory);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return result;
	}

	@Transactional(noRollbackFor = InvoTellPersistanceException.class)
	public SkillCategory updateSkillCategory(SkillCategory skillCategory) throws InvoTellPersistanceException {
		LOGGER.info("Inside the SkillCategoryRepository->updateSkillCategory(SkillCategory skillCategory)");
		SkillCategory result = null;
		try {
			Optional<SkillCategory> optional = skillCategoryJpaRepository
					.findOne(QSkillCategory.skillCategory.skillCategoryID.eq(skillCategory.getSkillCategoryID()));
			if (optional.isPresent()) {
				SkillCategory proxy = optional.get();
				proxy.setName(skillCategory.getName());
				result = skillCategoryJpaRepository.save(proxy);
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return result;
	}

	@Transactional(readOnly = true)
	public SkillCategory getSkillCategoryBySkillId(Long skillCategoryId) throws InvoTellPersistanceException {
		LOGGER.info("Inside the SkillCategoryRepository->getSkillCategoryBySkillId(Long skillCategoryId)");
		SkillCategory result = null;
		try {
			Optional<SkillCategory> optional = skillCategoryJpaRepository
					.findOne(QSkillCategory.skillCategory.skillCategoryID.eq(skillCategoryId));
			if (optional.isPresent()) {
				result = optional.get();
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return result;
	}

	@Transactional(readOnly = true)
	public SkillCategory getSkillCategoryBySkillName(String skillCategoryName) throws InvoTellPersistanceException {
		LOGGER.info("Inside the SkillCategoryRepository->getSkillCategoryBySkillName(String skillCategoryName)");
		SkillCategory result = null;
		try {
			Optional<SkillCategory> optional = skillCategoryJpaRepository
					.findOne(QSkillCategory.skillCategory.name.eq(skillCategoryName));
			if (optional.isPresent()) {
				result = optional.get();
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return result;
	}

	@Transactional(noRollbackFor = InvoTellPersistanceException.class)
	public void deleteSkillCategory(Long skillCategoryId) throws InvoTellPersistanceException {
		LOGGER.info("Inside the deleteSkillCategory(Long skillCategoryId)");
		try {
			Iterable<Skill> skillList = skillJpaRepository.findAll(QSkill.skill.skillcategoryid.eq(skillCategoryId));
			skillJpaRepository.deleteAll(skillList);
			Optional<SkillCategory> optional = skillCategoryJpaRepository
					.findOne(QSkillCategory.skillCategory.skillCategoryID.eq(skillCategoryId));
			if (optional.isPresent()) {
				SkillCategory result = optional.get();
				skillCategoryJpaRepository.delete(result);
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
	}

	@Transactional(noRollbackFor = InvoTellPersistanceException.class)
	public void deleteSkillCategoryByName(String name) throws InvoTellPersistanceException {
		LOGGER.info("Inside the deleteSkillCategoryByName(String name)");
		try {
			Optional<SkillCategory> optional = skillCategoryJpaRepository
					.findOne(QSkillCategory.skillCategory.name.eq(name));
			if (optional.isPresent()) {
				SkillCategory result = optional.get();
				skillCategoryJpaRepository.delete(result);
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
	}

	@Transactional(readOnly = true)
	public List<SkillCategory> getSkillCategoryList(boolean showSkill) throws InvoTellPersistanceException {
		List<SkillCategory> result = null;
		try {
			result = skillCategoryJpaRepository.findAll();
			if (showSkill && (result != null && result.size() > 0)) {
				Iterator<SkillCategory> it = result.iterator();
				while (it.hasNext()) {
					SkillCategory skillCategory = (SkillCategory) it.next();
					List<Skill> tempSkillList = new ArrayList<Skill>();
					Iterable<Skill> skillList = skillJpaRepository.findAll(QSkill.skill.skillcategoryid.eq(skillCategory.getSkillCategoryID()));
					skillList.forEach(skill->{
						tempSkillList.add(skill);
					});
					skillCategory.setSkill(tempSkillList);					
				}
			}
			entityManager.clear();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return result;
	}

}
