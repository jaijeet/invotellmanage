package com.invotell.box.dao.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import com.invotell.box.dao.entity.CPU;

public interface CPUJpaRepository  extends JpaRepository<CPU, Long>, QuerydslPredicateExecutor<CPU>{

}
