package com.invotell.box.dao.repository;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.invotell.box.comn.exception.InvoTellPersistanceException;
import com.invotell.box.dao.entity.IVRInfo;
import com.invotell.box.dao.entity.QIVRInfo;
import com.invotell.box.dao.entity.QQueueInfo;
import com.invotell.box.dao.entity.QueueInfo;
import com.invotell.box.dao.repository.jpa.QueueInfoJpaRepository;

@Repository
public class QueueInfoRepository {
	private static final Logger LOGGER = LoggerFactory.getLogger(QueueInfoRepository.class);
	
	@Autowired
	private QueueInfoJpaRepository queueInfoRepository;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Transactional(noRollbackFor = InvoTellPersistanceException.class)
	public QueueInfo saveQueueInfo(QueueInfo queueInfo) throws InvoTellPersistanceException 
	{
		QueueInfo result=null;
		try {
			
		} catch (Exception e) {
			throw new InvoTellPersistanceException(e.getMessage(), e.getCause());
		}
		return result;
	}
	
	@Transactional(readOnly = true)
	public QueueInfo getQueueInfo() throws InvoTellPersistanceException
	{
		QueueInfo result=null;
		try {
			
		} catch (Exception e) {
			throw new InvoTellPersistanceException(e.getMessage(), e.getCause());
		}
		return result;
	}
	
	@Transactional(noRollbackFor = InvoTellPersistanceException.class)
	public void deleteIvrInfo(QueueInfo ivrInfo) throws InvoTellPersistanceException
	{
		try {
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
	}
	
	@Transactional(noRollbackFor = InvoTellPersistanceException.class)
	public boolean addQueueInfoList(Long qid,List<QueueInfo> queueInfos) throws InvoTellPersistanceException{
		boolean result = false;
		try {			
			for (QueueInfo info : queueInfos) {			
				queueInfoRepository.save(info);
			}	
			result=true;
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return result;	
	}
}
