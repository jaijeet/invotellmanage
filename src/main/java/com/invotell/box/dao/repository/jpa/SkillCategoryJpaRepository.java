package com.invotell.box.dao.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import com.invotell.box.dao.entity.SkillCategory;

public interface SkillCategoryJpaRepository extends JpaRepository<SkillCategory, Long>, QuerydslPredicateExecutor<SkillCategory>{
	
	@Query("Select max(skillCategoryID) as nodeID from SkillCategory")
	Integer getMaxSkillCategoryId(); 
}
