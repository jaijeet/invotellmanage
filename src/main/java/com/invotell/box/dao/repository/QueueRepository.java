package com.invotell.box.dao.repository;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.invotell.box.comn.exception.InvoTellPersistanceException;
import com.invotell.box.dao.entity.Queue;
import com.invotell.box.dao.entity.QQueue;
import com.invotell.box.dao.repository.jpa.QueueJpaRepository;

@Repository
public class QueueRepository {
	private static final Logger LOGGER = LoggerFactory.getLogger(QueueRepository.class);

	@Autowired
	private QueueJpaRepository queueJpaRepository;

	@PersistenceContext
	private EntityManager entityManager;

	@Transactional(noRollbackFor = InvoTellPersistanceException.class)
	public Queue saveQueue(Queue queueToSave) throws InvoTellPersistanceException {
		LOGGER.info("Inside the QueueRepository->saveIvrInfo(Queue queueToSave)");
		Queue queue = null;
		try {
			queue = queueJpaRepository.save(queueToSave);
			entityManager.clear();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return queue;
	}

	public Queue getQueue(Long ivrID, Long queueID) throws InvoTellPersistanceException {
		LOGGER.info("Inside the QueueRepository->getIvrInfo(Long ivrID, Long queueID)");
		Queue result = null;
		try {
			Optional<Queue> Queue = queueJpaRepository.findOne(QQueue.queue.queueID.eq(queueID).and(QQueue.queue.ivrId.eq(ivrID)));
			if (Queue.isPresent()) {
				result = Queue.get();
			}
			entityManager.clear();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return result;
	}
	
	@Transactional(noRollbackFor = InvoTellPersistanceException.class)
	public void deleteQueue(Queue queue) throws InvoTellPersistanceException
	{
		try {
			queueJpaRepository.delete(queue);
			entityManager.clear();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
	}
}
