package com.invotell.box.dao.repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.invotell.box.comn.exception.InvoTellPersistanceException;
import com.invotell.box.dao.entity.QSkill;
import com.invotell.box.dao.entity.Skill;
import com.invotell.box.dao.entity.SkillCategory;
import com.invotell.box.dao.modal.SkillModel;
import com.invotell.box.dao.repository.jpa.SkillCategoryJpaRepository;
import com.invotell.box.dao.repository.jpa.SkillJpaRepository;

@Repository
public class SkillRepository {
	private static final Logger LOGGER = LoggerFactory.getLogger(SkillRepository.class);

	@Autowired
	private SkillJpaRepository skillJpaRepository; 
	
	@Autowired
	private SkillCategoryJpaRepository skillCategoryJpaRepository; 
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Transactional(noRollbackFor = InvoTellPersistanceException.class)
	public Skill saveNewSkill(Skill skillToSave) throws InvoTellPersistanceException{
		Skill result = null;
		try {
			result = skillJpaRepository.save(skillToSave);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return result;
	}
	
	@Transactional(readOnly = true)
	public Long getMaxSkillId() throws InvoTellPersistanceException {
		Long skillId = null;
		try {
			skillId = skillJpaRepository.getMaxSkillId();
			entityManager.clear();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return skillId;
	}
	
	@Transactional(readOnly = true)
	public Skill getSkillBySkillId(Long skillId,boolean clearEntityManager) throws InvoTellPersistanceException {
		Skill skill = null;
		try {
			Optional<Skill> optional = skillJpaRepository.findOne(QSkill.skill.skillId.eq(skillId));
			if(optional.isPresent()) {
				skill = optional.get();
			}
			
			if(clearEntityManager) {
				entityManager.clear();
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return skill;
	}
	
	@Transactional(readOnly = true)
	public List<Skill> getSkillListByCategoryId(Long catId) throws InvoTellPersistanceException {
		List<Skill> skill = new ArrayList<Skill>();
		try {
			
			Iterable<Skill>  iterable = skillJpaRepository.findAll(QSkill.skill.skillcategoryid.eq(catId));
			iterable.forEach(temp->{
				skill.add(temp);
			});
			entityManager.clear();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return skill;
	}
		
	@Transactional(noRollbackFor = InvoTellPersistanceException.class)
	public void deletedSkillById(Long id) throws InvoTellPersistanceException {
		try {
			Optional<Skill> optional = skillJpaRepository.findOne(QSkill.skill.skillId.eq(id));
			if(optional.isPresent()) {
				Skill proxy = optional.get();
				skillJpaRepository.delete(proxy);
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}		
	}
	
	@Transactional(noRollbackFor = InvoTellPersistanceException.class)
	public Skill updateSkill(Skill skillToUpdate,Long skillId,Long skillcategoryid) throws InvoTellPersistanceException {
		Skill result=null;
		try {
			Optional<Skill> optional = skillJpaRepository.findOne(QSkill.skill.skillId.eq(skillId));
			if(optional.isPresent()) {
				Skill proxy = optional.get();
				proxy.setName(skillToUpdate.getName());
				proxy.setDescription(skillToUpdate.getDescription());
				proxy.setSkillcategoryid(skillcategoryid);
				skillJpaRepository.save(proxy);
				result = proxy;
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}	
		return result;
	}

	public List<SkillModel> getSkillList()  throws InvoTellPersistanceException {
		List<SkillModel> result=new ArrayList<SkillModel>();
		try {
			List<Skill> list = skillJpaRepository.findAll();
			List<SkillCategory> catList = skillCategoryJpaRepository.findAll();
			Iterator<Skill> itSkill = list.iterator();
			while (itSkill.hasNext()) {
				Skill skill = (Skill) itSkill.next();
				Iterator<SkillCategory> itSkillCat = catList.iterator();
				while (itSkillCat.hasNext()) {
					SkillCategory skillCategory = (SkillCategory) itSkillCat.next();
					if(skillCategory.getSkillCategoryID() == skill.getSkillcategoryid()) {
						SkillModel model = new SkillModel();
						model.setDescription(skill.getDescription());
						model.setName(skill.getName());
						model.setSkillcategory(skillCategory.getName());
						model.setSkillId(skill.getSkillId());
						model.setSkillCategoryId(skill.getSkillcategoryid());
						result.add(model);
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}	
		return result;
	}
}
