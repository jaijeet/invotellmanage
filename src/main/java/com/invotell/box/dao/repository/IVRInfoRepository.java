package com.invotell.box.dao.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.invotell.box.comn.exception.InvoTellPersistanceException;
import com.invotell.box.dao.entity.IVR;
import com.invotell.box.dao.entity.IVRInfo;
import com.invotell.box.dao.entity.QIVRInfo;
import com.invotell.box.dao.repository.jpa.IVRInfoJpaRepository;
import com.querydsl.jpa.impl.JPADeleteClause;

@Repository
public class IVRInfoRepository {
	private static final Logger LOGGER = LoggerFactory.getLogger(IVRInfoRepository.class);
	
	@Autowired
	private IVRInfoJpaRepository ivrInfoJpaRepository;

	@PersistenceContext
	private EntityManager entityManager;

	@Transactional(noRollbackFor = InvoTellPersistanceException.class)
	public IVRInfo saveIvrInfo(IVRInfo ivrInfoToSave, boolean clearSession) throws InvoTellPersistanceException {
		LOGGER.info("Inside the IVRInfoRepository->saveIvrInfo(Ivr ivrInfoToSave)");
		IVRInfo ivrInfo = null;
		try {
			ivrInfo = ivrInfoJpaRepository.save(ivrInfoToSave);
			if(clearSession) {
				entityManager.clear();
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return ivrInfo;
	}
	
	@Transactional(readOnly = true)
	public IVRInfo getIvrInfo(Long ivrID,String keyword, boolean clearSession) throws InvoTellPersistanceException {
		LOGGER.info("Inside the IVRInfoRepository->getIvrInfo(Long ivrID,String keyword)");
		IVRInfo result = null;
		try {
			Optional<IVRInfo> ivrInfo = ivrInfoJpaRepository.findOne(QIVRInfo.iVRInfo.keyword.eq(keyword).and(QIVRInfo.iVRInfo.ivrID.eq(ivrID)));
			if(ivrInfo.isPresent())
			{
				result = ivrInfo.get();
			}
			if(clearSession) {
				entityManager.clear();
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return result;
	}
	
	@Transactional(noRollbackFor = InvoTellPersistanceException.class)
	public void deleteIvrInfo(IVRInfo ivrInfo, boolean clearSession) throws InvoTellPersistanceException
	{
		try {
			ivrInfoJpaRepository.delete(ivrInfo);
			if(clearSession) {
				entityManager.clear();
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
	}
	
	@Transactional(noRollbackFor = InvoTellPersistanceException.class)
	public void deleteIvrInfoByKeyword(String keyword,Long ivrId, boolean clearSession) throws InvoTellPersistanceException
	{
		try {
			QIVRInfo ivrInfo = QIVRInfo.iVRInfo; 
			new JPADeleteClause(entityManager, ivrInfo).where(ivrInfo.ivrID.eq(ivrId).and(ivrInfo.keyword.eq(keyword))).execute();
			if(clearSession) {
				entityManager.clear();
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
	}
	
	@Transactional(noRollbackFor = InvoTellPersistanceException.class)
	public void deleteHolidayList(Long ivrId, boolean clearSession) throws InvoTellPersistanceException
	{
		try {
			QIVRInfo ivrInfo = QIVRInfo.iVRInfo; 
			new JPADeleteClause(entityManager, ivrInfo).where(ivrInfo.ivrID.eq(ivrId).and(ivrInfo.keyword.contains("holiday"))).execute();
			if(clearSession) {
				entityManager.clear();
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
	}
	
	@Transactional(noRollbackFor = InvoTellPersistanceException.class)
	public void updateIvrInfo(IVRInfo ivrInfo,IVRInfo ivrToUpdate, boolean clearSession) throws InvoTellPersistanceException
	{
		try {
			Optional<IVRInfo> ivrInfoOption = ivrInfoJpaRepository.findOne(QIVRInfo.iVRInfo.keyword.eq(ivrInfo.getKeyword()).and(QIVRInfo.iVRInfo.ivrID.eq(ivrInfo.getIvrID())));
			if(ivrInfoOption.isPresent()) {
				IVRInfo proxy = ivrInfoOption.get();
				proxy.setValue(ivrToUpdate.getValue());
				ivrInfoJpaRepository.save(proxy);
				if(clearSession) {
					entityManager.clear();
				}
			}else {
				throw new InvoTellPersistanceException("IvrInfo with keyword ["+ivrInfo.getKeyword()+"] and ivrId ["+ivrInfo.getIvrID()+"] is not available in database");
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
	}
	
	@Transactional(readOnly = true)
	public List<IVRInfo> getIvrInfoList(Long ivrID, boolean clearSession) throws InvoTellPersistanceException {
		LOGGER.info("Inside the IVRInfoRepository->getIvrInfoList(IvrId)");
		List<IVRInfo> result = new ArrayList<IVRInfo>();
		try {
			Iterable<IVRInfo> ivrInfo = ivrInfoJpaRepository.findAll(QIVRInfo.iVRInfo.ivrID.eq(ivrID));//(QIVRInfo.iVRInfo.keyword.eq(keyword).and(QIVRInfo.iVRInfo.ivrID.eq(ivrID)));
			ivrInfo.forEach( info->{
				result.add(info);
			});
			if(clearSession) {
				entityManager.clear();
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return result;
	}

	@Transactional(noRollbackFor = InvoTellPersistanceException.class)
	public boolean updateIvrInfoList(Long ivrID,List<IVRInfo> ivrToUpdate, boolean clearSession) throws InvoTellPersistanceException {
		LOGGER.info("Inside the IVRInfoRepository->saveIvrInfo(Ivr ivrInfoToSave)");
		boolean result = false;
		try {			
			for (IVRInfo info : ivrToUpdate) {				
				Optional<IVRInfo> ivrInfoOption = ivrInfoJpaRepository.findOne(QIVRInfo.iVRInfo.keyword.eq(info.getKeyword()).and(QIVRInfo.iVRInfo.ivrID.eq(ivrID)));
				if(ivrInfoOption.isPresent()) {
					IVRInfo proxy = ivrInfoOption.get();
					proxy.setValue(info.getValue());
					ivrInfoJpaRepository.save(proxy);
				}else {
					info.setIvrID(ivrID);
					ivrInfoJpaRepository.save(info);
				}
			}	
			result=true;
			if(clearSession) {
				entityManager.clear();
			}
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return result;
	}
}
