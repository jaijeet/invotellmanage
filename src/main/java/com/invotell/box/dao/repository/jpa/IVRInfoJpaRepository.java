package com.invotell.box.dao.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import com.invotell.box.dao.entity.IVRInfo;

public interface IVRInfoJpaRepository extends JpaRepository<IVRInfo, Long>, QuerydslPredicateExecutor<IVRInfo>{
	
}
