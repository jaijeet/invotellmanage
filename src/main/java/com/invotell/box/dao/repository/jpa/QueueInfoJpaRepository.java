package com.invotell.box.dao.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import com.invotell.box.dao.entity.QueueInfo;

public interface QueueInfoJpaRepository extends JpaRepository<QueueInfo, Long>, QuerydslPredicateExecutor<QueueInfo>{

}
