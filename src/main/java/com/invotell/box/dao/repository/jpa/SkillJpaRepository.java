package com.invotell.box.dao.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import com.invotell.box.dao.entity.Skill;

public interface SkillJpaRepository extends JpaRepository<Skill, Long>, QuerydslPredicateExecutor<Skill>{

	@Query("Select max(skillId) as nodeID from Skill")
	Long getMaxSkillId(); 
}
