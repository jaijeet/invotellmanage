package com.invotell.box.dao.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import com.invotell.box.dao.entity.PbxIvrTree;

public interface PbxIvrTreeJpaRepository extends JpaRepository<PbxIvrTree, Long>, QuerydslPredicateExecutor<PbxIvrTree>{

	
	@Query("Select max(nodeid) as nodeID from PbxIvrTree  where  ivrid=?1")
	Integer getMaxNodeIdByIvrId(Long ivrId); 
}
