package com.invotell.box.dao.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import com.invotell.box.dao.entity.UserSkill;

public interface UserSkillJpaRepository extends JpaRepository<UserSkill, Long>,QuerydslPredicateExecutor<UserSkill>{

}
