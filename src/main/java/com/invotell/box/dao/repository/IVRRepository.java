package com.invotell.box.dao.repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.invotell.box.comn.exception.InvoTellPersistanceException;
import com.invotell.box.dao.entity.IVR;
import com.invotell.box.dao.entity.IVRInfo;
import com.invotell.box.dao.entity.QIVR;
import com.invotell.box.dao.entity.QIVRInfo;
import com.invotell.box.dao.entity.QQueue;
import com.invotell.box.dao.entity.QQueueInfo;
import com.invotell.box.dao.entity.Queue;
import com.invotell.box.dao.entity.QueueInfo;
import com.invotell.box.dao.repository.jpa.IVRInfoJpaRepository;
import com.invotell.box.dao.repository.jpa.IVRJpaRepository;
import com.invotell.box.dao.repository.jpa.QueueInfoJpaRepository;
import com.invotell.box.dao.repository.jpa.QueueJpaRepository;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAUpdateClause;

@Repository
public class IVRRepository {
	private static final Logger LOGGER = LoggerFactory.getLogger(IVRRepository.class);

	@Autowired
	private IVRJpaRepository ivrJpaRepository;
	
	@Autowired
	private IVRInfoJpaRepository ivrInfoJpaRepository;
	
	@Autowired
	private QueueJpaRepository queueJpaRepository;

	@Autowired
	private QueueInfoJpaRepository queueInfoJpaRepository;
	
	@PersistenceContext
	private EntityManager entityManager;

	@Transactional(noRollbackFor = InvoTellPersistanceException.class)
	public IVR saveIvr(IVR ivrToSave) throws InvoTellPersistanceException {
		LOGGER.info("Inside the IVRRepository->addNewIvr(Ivr ivrToSave)");
		IVR ivr = null;
		try {
			ivr = ivrJpaRepository.saveAndFlush(ivrToSave);
			entityManager.clear();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return ivr;
	}

	@Transactional(readOnly = true)
	public IVR getIvrByIvrId(Long ivrId, boolean ivrInfo, boolean queue) throws InvoTellPersistanceException {
		IVR result = null;
		try {
			BooleanExpression expression = QIVR.iVR.ivrID.eq(ivrId);
			Optional<IVR> ivr = ivrJpaRepository.findOne(expression);
			if (ivr.isPresent()) {
				result = ivr.get();
			}

			if (result != null) {
				if (ivrInfo) {
					List<IVRInfo> list = new ArrayList<IVRInfo>();
					Iterable<IVRInfo> iterable = ivrInfoJpaRepository.findAll(QIVRInfo.iVRInfo.ivrID.eq(ivrId));//(QIVRInfo.iVRInfo.keyword.eq(keyword).and(QIVRInfo.iVRInfo.ivrID.eq(ivrID)));
					iterable.forEach( info->{
						list.add(info);
					});
					result.setIvrInfo(list);
				}

				if (queue) {
					List<Queue> list = new ArrayList<Queue>();
					Iterable<Queue> iterable = queueJpaRepository.findAll(QQueue.queue.ivrId.eq(ivrId));//(QIVRInfo.iVRInfo.keyword.eq(keyword).and(QIVRInfo.iVRInfo.ivrID.eq(ivrID)));
					iterable.forEach( que->{
						list.add(que);
					});
					result.setQueues(list);
				}
			}
			entityManager.clear();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return result;
	}
	
	@Transactional(noRollbackFor = InvoTellPersistanceException.class)
	public void deleteIvr(IVR ivr) throws InvoTellPersistanceException
	{
		try {
			
			ivr = getIvrByIvrId(ivr.getIvrID(), false, false);
			
			Iterable<IVRInfo> infos = ivrInfoJpaRepository.findAll(QIVRInfo.iVRInfo.ivrID.eq(ivr.getIvrID()));
			System.out.println(infos);
			ivrInfoJpaRepository.deleteAll(infos);			
			
			Iterable<Queue> queues = queueJpaRepository.findAll(QQueue.queue.ivrId.eq(ivr.getIvrID())); 
			queues.forEach(queue->{
				Iterable<QueueInfo> queueInfos = queueInfoJpaRepository.findAll(QQueueInfo.queueInfo.qid.eq(queue.getQueueID())); 
				queueInfoJpaRepository.deleteAll(queueInfos);
			});
			queueJpaRepository.deleteAll(queues);
			
			ivrJpaRepository.delete(ivr);
//			entityManager.clear();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
//		https://mydesktop-west04.att.com
	}

	@Transactional(noRollbackFor = InvoTellPersistanceException.class)
	public void updateIvr(IVR ivr,Long ivrId) throws InvoTellPersistanceException
	{
		try {  
			IVR proxy = null;
			BooleanExpression expression = QIVR.iVR.ivrID.eq(ivrId);
			Optional<IVR> ivrOptional = ivrJpaRepository.findOne(expression);
			if (ivrOptional.isPresent()) {
				proxy = ivrOptional.get();
			}
			this.copyIvrData(proxy,ivr);
			ivrJpaRepository.save(proxy);
			entityManager.clear();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
	}
	
	@Transactional(readOnly = true)
	public List<IVR> getIvrList() throws InvoTellPersistanceException {
		List<IVR> result = null;
		try {
			result = ivrJpaRepository.findAll();
			Iterator<IVR> ivrIterator = result.iterator();
			while(ivrIterator.hasNext())
			{
				IVR temp = ivrIterator.next();
				Iterator<IVRInfo> ivrInfoIterator = temp.getIvrInfo().iterator();
				while (ivrInfoIterator.hasNext()) {
					ivrInfoIterator.next();
				}
				Iterator<Queue> queueIterator = temp.getQueues().iterator();
				while (queueIterator.hasNext()) {
					queueIterator.next();
				}
			}
			entityManager.clear();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return result;
	}
	
	private void copyIvrData(IVR proxy, IVR ivr) {
		proxy.setIvrID(ivr.getIvrID());
		proxy.setCreatedBy(ivr.getCreatedBy());
		proxy.setCreatedOn(ivr.getCreatedOn());
		proxy.setIsDeleted(ivr.getIsDeleted());
		proxy.setIvrInfo(ivr.getIvrInfo());
		proxy.setIvrName(ivr.getIvrName());
		proxy.setQueues(ivr.getQueues());
	}
}
