package com.invotell.box.dao.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.invotell.box.dao.constant.PersistanceConstant;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = PersistanceConstant.SKILL_TABLE)
public class Skill {

	@JsonIgnore
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "skillid")
	private Long skillId;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;

	@JsonIgnore
	@Column(name = "skillcategoryid")
	private Long skillcategoryid;

}
