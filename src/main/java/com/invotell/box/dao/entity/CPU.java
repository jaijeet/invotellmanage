package com.invotell.box.dao.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.invotell.box.dao.constant.PersistanceConstant;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = PersistanceConstant.CPU_USAGE)
public class CPU implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@JsonIgnore
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "timedata")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date time;
	
	@Column(name="usagedata")
	private Float usage;
}
