package com.invotell.box.dao.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.invotell.box.dao.constant.PersistanceConstant;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = PersistanceConstant.IVR_TABLE)
public class IVR implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonIgnore
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;

	@Column(name="ivr_id")
	private Long ivrID;
	
	@Column(name="ivr_name")
	private String ivrName;
	
	@Column(name="created_on")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createdOn;
	
	@Column(name="created_by")
	private String createdBy;
	
	@Column(name="is_deleted")
	private Boolean isDeleted;

	@OneToMany(fetch = FetchType.LAZY,orphanRemoval = true)
	@Cascade({CascadeType.ALL})
	@JoinColumn(name = "ivr_id",insertable = true,updatable = false)
	private List<IVRInfo> ivrInfo = new ArrayList<IVRInfo>(0);

	@OneToMany(fetch = FetchType.LAZY,orphanRemoval = true)
	@Cascade({CascadeType.ALL})
	@JoinColumn(name = "ivr_id",insertable = false,updatable = false)
	private List<Queue> queues;
	
}
