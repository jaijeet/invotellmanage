package com.invotell.box.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.invotell.box.dao.constant.PersistanceConstant;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = PersistanceConstant.USER_SKILL)
public class UserSkill {
	
	@JsonIgnore
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="skillid")
	private Long skillId;
	
	@Column(name="userid")
	private String userId;
	
	@Transient
	private Skill skill;
}
