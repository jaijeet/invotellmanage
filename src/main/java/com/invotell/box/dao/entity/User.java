package com.invotell.box.dao.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.invotell.box.dao.constant.PersistanceConstant;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = PersistanceConstant.USER_TABLE)
public class User implements Serializable {

	private static final long serialVersionUID = 1L;
	@JsonIgnore
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "phone")
	private Long phone;

	@Column(unique = true, name = "email")
	private String email;

	@Column(name = "password")
	private String password;

	@Column(unique = true, name = "extension")
	private Integer extension;

	@Column(unique = true, name = "sip_id")
	private String sipID;

	@Column(name = "timezone")
	private String timeZone;

	@Column(name = "userid")
	private String userId;

	@OneToMany(fetch = FetchType.EAGER,orphanRemoval = true)
	@Cascade({CascadeType.ALL})
	@JoinColumn(name = "userid",insertable = true,updatable = false)
	private List<UserSkill> userSkill = new ArrayList<UserSkill>(0);

}
