package com.invotell.box.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.invotell.box.dao.constant.PersistanceConstant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = PersistanceConstant.PBX_IVR_TREE_TABLE)
public class PbxIvrTree {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonIgnore
	@Column(name = "id")
	private Long id;
	
	@Column(name = "ivr_id")
	private Long ivrid;
	
	@Column(name="node_id")
	private Integer nodeid;
	
	@Column(name="node_type")
	private String nodeType;
	
	@Column(name="name")
	private String name;
	
	@Column(name="key_press")
	private Byte keyPress;
	
	@Column(name="parent_node_id")
	private Integer parenetNode;
	
	@Column(name="prompt_file")
	private String promtFile;
	
	@Column(name="qid")
	private Long qid;
	
	@Column(name="prompt_status")
	private Boolean promptStatus;
}
