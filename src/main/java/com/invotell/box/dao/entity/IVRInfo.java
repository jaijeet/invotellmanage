package com.invotell.box.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.invotell.box.dao.constant.PersistanceConstant;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = PersistanceConstant.IVR_INFO_TABLE)
public class IVRInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonIgnore
	@Column(name="id")
	public Long id;
	
	@Column(name="keyword")
	public String keyword;
	
	@Column(name="value")
	public String value;

	@JsonIgnore
	@Column(name="ivr_id")
	public Long ivrID;

}
