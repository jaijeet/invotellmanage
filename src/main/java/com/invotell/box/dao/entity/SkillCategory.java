package com.invotell.box.dao.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.invotell.box.dao.constant.PersistanceConstant;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = PersistanceConstant.SKILL_CATEGORY_TABLE)
public class SkillCategory {
	@JsonIgnore
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name="skillcategoryid")
	private Long skillCategoryID;
	
	@Column(name="skillcategoryname")
	private String name;
	
	@OneToMany(fetch = FetchType.LAZY,orphanRemoval = true)
	@Cascade({CascadeType.ALL})
	@JoinColumn(name = "skillcategoryid",insertable = true,updatable = false)
	private List<Skill> skill = new ArrayList<Skill>(0);
}
