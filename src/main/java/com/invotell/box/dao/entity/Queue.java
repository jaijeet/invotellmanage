package com.invotell.box.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.invotell.box.dao.constant.PersistanceConstant;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = PersistanceConstant.QUEUE_TABLE)
public class Queue {

	@JsonIgnore
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	@Column(name="queue_id")
	private Long queueID;
	
	@Column(name="queue_name")
	private String queueName;
	
	@Column(name="on_key")
	private Byte onKey;
	
	@JsonIgnore
	@Column(name = "ivr_id")
	public Long ivrId;
}
