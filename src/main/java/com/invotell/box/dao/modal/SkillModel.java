package com.invotell.box.dao.modal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SkillModel {
	
	private Long skillId;
	private String name;
	private String description;
	private String skillcategory;
	private Long skillCategoryId;
}
