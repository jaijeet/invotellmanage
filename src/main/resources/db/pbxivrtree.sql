CREATE TABLE `pbxivrtree` (
  `id` bigint(20) NOT NULL,
  `ivr_id` bigint(20),
  `node_id` tinyint(4),
  `node_type` varchar(50),
  `key_press` tinyint(4) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `parent_node_id` tinyint(4),
  `prompt_file` varchar(100) DEFAULT NULL,
  `qid` bigint(20) default NULL, 
  `prompt_status` tinyint(4) default '1' ,
	CONSTRAINT `pk_pbxivrtree` PRIMARY KEY (`id`),
	CONSTRAINT `uniq_pbxivrtree` UNIQUE (`ivr_id`,`key_press`,`qid`,`parent_node_id`)
);

ALTER TABLE `pbxivrtree` CHANGE COLUMN `id` `id` BIGINT(20) NOT NULL AUTO_INCREMENT ;