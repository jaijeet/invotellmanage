CREATE TABLE `springmysql`.`userskill` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `skillid` INT NOT NULL,
  `userid` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `USER_ID_FK_REF_idx` (`userid` ASC),
  CONSTRAINT `USER_ID_FK_REF` FOREIGN KEY (`userid`) REFERENCES `springmysql`.`user` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION
 );

ALTER TABLE `springmysql`.`userskill` ADD INDEX `SKILL_ID_FK_REF_idx` (`skillid` ASC); 