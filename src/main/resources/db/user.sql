CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `phone` bigint(20) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `extension` int(10) DEFAULT NULL,
  `sip_id` varchar(20) DEFAULT NULL,
  CONSTRAINT `pk_user` PRIMARY KEY (`id`),
  CONSTRAINT `uq_user_email` UNIQUE KEY (`email`),
  CONSTRAINT `uq_user_extension` UNIQUE KEY (`extension`),
  CONSTRAINT `uq_user_sip_id` UNIQUE KEY (`sip_id`)
);

ALTER TABLE `springmysql`.`user` CHANGE COLUMN `id` `id` BIGINT(20) NOT NULL AUTO_INCREMENT ;

ALTER TABLE `springmysql`.`user` ADD COLUMN `timezone` VARCHAR(5) NULL AFTER `sip_id`;

ALTER TABLE `springmysql`.`user` ADD COLUMN `userid` VARCHAR(10) NULL DEFAULT NULL AFTER `timezone`, ADD UNIQUE INDEX `userid_UNIQUE` (`userid` ASC);
