CREATE TABLE `springmysql`.`usagecputable` (
  `id` BIGINT(20) NOT NULL,
  `timedata` datetime NULL,
  `usagedata` INT(3) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC));
  
ALTER TABLE `usagecputable` CHANGE COLUMN `id` `id` BIGINT(20) NOT NULL AUTO_INCREMENT ;
  