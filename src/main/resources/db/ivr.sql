CREATE TABLE `ivr` (
  `id` bigint(20) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  `ivr_id` bigint(20) DEFAULT NULL,
  `ivr_name` varchar(255) DEFAULT NULL,
  CONSTRAINT `pk_ivr_id` PRIMARY KEY (`id`),
  CONSTRAINT `uniq_ivr_ivrid` UNIQUE (`ivr_id`)
);

ALTER TABLE `ivr` CHANGE COLUMN `id` `id` BIGINT(20) NOT NULL AUTO_INCREMENT ;
