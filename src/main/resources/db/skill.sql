CREATE TABLE `springmysql`.`skill` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL,
  `description` VARCHAR(1000) NULL,
  `skillid` INT NOT NULL,
  `skillcategoryid` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `skillcategoryFK_idx` (`skillcategoryid` ASC),
  CONSTRAINT `skillcategoryFK` FOREIGN KEY (`skillcategoryid`) REFERENCES `springmysql`.`skillcategory` (`skillcategoryid`));

  
  ALTER TABLE `springMysql`.`skill` ADD UNIQUE INDEX `skillid_UNIQUE` (`skillid` ASC);
