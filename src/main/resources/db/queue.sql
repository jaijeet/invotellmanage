CREATE TABLE `queue` (
  `id` bigint(20) NOT NULL,
  `ivr_id` bigint(20),
  `on_key` tinyint(4) DEFAULT NULL,
  `queue_id` bigint(20) DEFAULT NULL,
  `queue_name` varchar(255) DEFAULT NULL,
  CONSTRAINT `pk_queue` PRIMARY KEY (`id`),
  CONSTRAINT `uniq_queue_ivid` UNIQUE (`ivr_id`,`queue_id`),	
  CONSTRAINT `fk_ivr_queue` FOREIGN KEY (`ivr_id`) REFERENCES `ivr`(`ivr_id`)
);

ALTER TABLE `queue` CHANGE COLUMN `id` `id` BIGINT(20) NOT NULL AUTO_INCREMENT ;
