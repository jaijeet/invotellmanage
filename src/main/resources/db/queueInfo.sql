CREATE TABLE `queueinfo` (
  `id` bigint(20) NOT NULL,
  `keyword` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `queue_id` bigint(20),
  CONSTRAINT `pk_queue_queueinfo` PRIMARY KEY (`id`)
);

ALTER TABLE `queueinfo` AUTO_INCREMENT=1;
ALTER TABLE `queueinfo` ADD CONSTRAINT `unique_queueinfo` UNIQUE (`queue_id`,`keyword`);