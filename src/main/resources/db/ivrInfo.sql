CREATE TABLE `ivrinfo` (
  `id` bigint(20) NOT NULL,
  `keyword` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `ivr_id` bigint(20),
  CONSTRAINT `pk_ivr_ivrinfo` PRIMARY KEY (`id`),	
  CONSTRAINT `fk_ivr_ivrinfo` FOREIGN KEY (`ivr_id`) REFERENCES `ivr`(`ivr_id`) 
);

ALTER TABLE `ivrinfo` CHANGE COLUMN `id` `id` BIGINT(20) NOT NULL AUTO_INCREMENT ;