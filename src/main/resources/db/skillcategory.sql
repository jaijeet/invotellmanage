CREATE TABLE `skillcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skillcategoryid` int(4) DEFAULT NULL,
  `skillcategoryname` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `skillcategoryid_UNIQUE` (`skillcategoryid`),
  UNIQUE KEY `skillcategoryname_UNIQUE` (`skillcategoryname`)
)
