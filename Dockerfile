# Use an official OpenJDK runtime as a parent image
FROM openjdk:17-jdk

WORKDIR /manage

# Copy the fat jar into the container at /app
COPY /target/invotell-0.0.1-SNAPSHOT.jar /manage/invotell.jar

# Make port 8080 available to the world outside this container
EXPOSE 8080

# Run jar file when the container launches
CMD ["java", "-jar", "invotell.jar"]